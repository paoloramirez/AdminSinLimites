package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;

public class InterestsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_interests)
    Toolbar toolbarInterests;
    @BindView(R.id.ch_fb)
    AppCompatCheckBox chFb;
    @BindView(R.id.ll_fb)
    LinearLayout llFb;
    @BindView(R.id.ch_instagram)
    AppCompatCheckBox chInstagram;
    @BindView(R.id.ll_instagram)
    LinearLayout llInstagram;
    @BindView(R.id.ch_twitter)
    AppCompatCheckBox chTwitter;
    @BindView(R.id.ll_twitter)
    LinearLayout llTwitter;
    @BindView(R.id.et_otros_redsocial)
    AppCompatEditText etOtrosRedsocial;
    @BindView(R.id.ch_smartphone1)
    AppCompatCheckBox chSmartphone1;
    @BindView(R.id.ll_smarthpone1)
    LinearLayout llSmarthpone1;
    @BindView(R.id.ch_tablet1)
    AppCompatCheckBox chTablet1;
    @BindView(R.id.ll_tablet1)
    LinearLayout llTablet1;
    @BindView(R.id.ch_laptop1)
    AppCompatCheckBox chLaptop1;
    @BindView(R.id.ll_laptop1)
    LinearLayout llLaptop1;
    @BindView(R.id.ch_pc1)
    AppCompatCheckBox chPc1;
    @BindView(R.id.ll_pc1)
    LinearLayout llPc1;
    @BindView(R.id.ch_smartphone2)
    AppCompatCheckBox chSmartphone2;
    @BindView(R.id.ll_smartphone2)
    LinearLayout llSmartphone2;
    @BindView(R.id.ch_tablet2)
    AppCompatCheckBox chTablet2;
    @BindView(R.id.ll_tablet2)
    LinearLayout llTablet2;
    @BindView(R.id.ch_laptop2)
    AppCompatCheckBox chLaptop2;
    @BindView(R.id.ll_laptop2)
    LinearLayout llLaptop2;
    @BindView(R.id.ch_pc2)
    AppCompatCheckBox chPc2;
    @BindView(R.id.ll_pc2)
    LinearLayout llPc2;

    @BindView(R.id.ch_futbol1)
    AppCompatCheckBox chFutbol1;
    @BindView(R.id.ll_futbol1)
    LinearLayout llFutbol1;
    @BindView(R.id.ch_voley1)
    AppCompatCheckBox chVoley1;
    @BindView(R.id.ll_voley1)
    LinearLayout llVoley1;
    @BindView(R.id.ch_bochas1)
    AppCompatCheckBox chBochas1;
    @BindView(R.id.ll_bochas1)
    LinearLayout llBochas1;
    @BindView(R.id.ch_atletismo1)
    AppCompatCheckBox chAtletismo1;
    @BindView(R.id.ll_atletismo1)
    LinearLayout llAtletismo1;


    @BindView(R.id.ch_natacion2)
    AppCompatCheckBox chNatacion2;
    @BindView(R.id.ll_natacion2)
    LinearLayout llNatacion2;
    @BindView(R.id.ch_atletismo2)
    AppCompatCheckBox chAtletismo2;
    @BindView(R.id.ll_atletismo2)
    LinearLayout llAtletismo2;
    @BindView(R.id.ch_pesas2)
    AppCompatCheckBox chPesas2;
    @BindView(R.id.ll_pesas2)
    LinearLayout llPesas2;
    @BindView(R.id.ch_basquet2)
    AppCompatCheckBox chBasquet2;
    @BindView(R.id.ll_basquet2)
    LinearLayout llBasquet2;
    @BindView(R.id.et_otros_deportes)
    AppCompatEditText etOtrosDeportes;
    @BindView(R.id.btn_interests)
    AppCompatButton btnInterests;


    int social_network = 0;
    String o_social_network = "";
    int device = 0;
    int pref_device = 0;
    int sports = 0;
    int pref_sports = 0;
    String o_pref_sports = "";

    //Add Status
    boolean o_sn = false;
    boolean o_ps = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interests);
        ButterKnife.bind(this);

        disableCheckBox();

        etOtrosRedsocial.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0 && o_sn) {
                    social_network-=8;
                    o_sn = false;
                } else {
                    if (!o_sn) {
                        social_network+=8;
                        o_sn = true;
                    }
                }
            }
        });

        etOtrosDeportes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0 && o_ps) {
                    pref_sports-=16;
                    o_ps = false;
                } else {
                    if (!o_ps) {
                        pref_sports+=16;
                        o_ps = true;
                    }
                }
            }
        });

        //Social Network
        chFb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    social_network += 1;
                } else {
                    social_network -= 1;
                }
            }
        });
        chInstagram.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    social_network += 2;
                } else {
                    social_network -= 2;
                }
            }
        });
        chTwitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    social_network += 4;
                } else {
                    social_network -= 4;
                }
            }
        });

        //Device
        chSmartphone1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    chSmartphone2.setEnabled(true);
                    device += 1;
                } else {
                    chSmartphone2.setEnabled(false);
                    chSmartphone2.setChecked(false);
                    pref_device = 0;
                    device -= 1;
                }
            }
        });
        chTablet1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    chTablet2.setEnabled(true);
                    device += 2;
                } else {
                    chTablet2.setEnabled(false);
                    chTablet2.setChecked(false);
                    pref_device = 0;
                    device -= 2;
                }
            }
        });
        chLaptop1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    chLaptop2.setEnabled(true);
                    device += 4;
                } else {
                    chLaptop2.setEnabled(false);
                    chLaptop2.setChecked(false);
                    pref_device = 0;
                    device -= 4;
                }
            }
        });
        chPc1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    chPc2.setEnabled(true);
                    device += 8;
                } else {
                    chPc2.setEnabled(false);
                    chPc2.setChecked(false);
                    pref_device = 0;
                    device -= 8;
                }
            }
        });

        //Preferences device
        chSmartphone2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_device = 1;
                    chTablet2.setChecked(false);
                    chLaptop2.setChecked(false);
                    chPc2.setChecked(false);
                } else {
                    pref_device = 0;
                }
            }
        });
        chTablet2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_device = 1;
                    chSmartphone2.setChecked(false);
                    chLaptop2.setChecked(false);
                    chPc2.setChecked(false);
                } else {
                    pref_device = 0;
                }
            }
        });
        chLaptop2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_device = 3;
                    chSmartphone2.setChecked(false);
                    chTablet2.setChecked(false);
                    chPc2.setChecked(false);
                } else {
                    pref_device = 0;
                }
            }
        });
        chPc2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_device = 4;
                    chSmartphone2.setChecked(false);
                    chTablet2.setChecked(false);
                    chLaptop2.setChecked(false);
                } else {
                    pref_device = 0;
                }
            }
        });

        //Sports
        chFutbol1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sports += 1;
                } else {
                    sports -= 1;
                }
            }
        });
        chVoley1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sports += 2;
                } else {
                    sports -= 2;
                }
            }
        });
        chBochas1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sports += 4;
                } else {
                    sports -= 4;
                }
            }
        });
        chAtletismo1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sports += 8;
                } else {
                    sports -= 8;
                }
            }
        });

        //Preferences Sports
        chNatacion2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_sports += 1;
                } else {
                    pref_sports -= 1;
                }
            }
        });
        chAtletismo2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_sports += 2;
                } else {
                    pref_sports -= 2;
                }
            }
        });
        chPesas2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_sports += 4;
                } else {
                    pref_sports -= 4;
                }
            }
        });
        chBasquet2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    pref_sports += 8;
                } else {
                    pref_sports -= 8;
                }
            }
        });

    }

    private void disableCheckBox() {
        chPc2.setEnabled(false);
        chLaptop2.setEnabled(false);
        chTablet2.setEnabled(false);
        chSmartphone2.setEnabled(false);
    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);

    }

    @OnClick({R.id.ll_fb, R.id.ll_instagram, R.id.ll_twitter, R.id.ll_smarthpone1, R.id.ll_tablet1, R.id.ll_laptop1, R.id.ll_pc1, R.id.ll_smartphone2, R.id.ll_tablet2, R.id.ll_laptop2, R.id.ll_pc2, R.id.ll_futbol1, R.id.ll_voley1, R.id.ll_bochas1, R.id.ll_atletismo1, R.id.ll_natacion2, R.id.ll_atletismo2, R.id.ll_pesas2, R.id.ll_basquet2, R.id.btn_interests})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_fb:
                if (chFb.isChecked()) {
                    chFb.setChecked(false);
                } else {
                    chFb.setChecked(true);
                }
                break;
            case R.id.ll_instagram:
                if (chInstagram.isChecked()) {
                    chInstagram.setChecked(false);
                } else {
                    chInstagram.setChecked(true);
                }
                break;
            case R.id.ll_twitter:
                if (chTwitter.isChecked()) {
                    chTwitter.setChecked(false);
                } else {
                    chTwitter.setChecked(true);
                }
                break;
            case R.id.ll_smarthpone1:
                if (chSmartphone1.isChecked()) {
                    chSmartphone1.setChecked(false);
                } else {
                    chSmartphone1.setChecked(true);
                }
                break;
            case R.id.ll_tablet1:
                if (chTablet1.isChecked()) {
                    chTablet1.setChecked(false);
                } else {
                    chTablet1.setChecked(true);
                }
                break;
            case R.id.ll_laptop1:
                if (chLaptop1.isChecked()) {
                    chLaptop1.setChecked(false);
                } else {
                    chLaptop1.setChecked(true);
                }
                break;
            case R.id.ll_pc1:
                if (chPc1.isChecked()) {
                    chPc1.setChecked(false);
                } else {
                    chPc1.setChecked(true);
                }
                break;
            case R.id.ll_smartphone2:
                if (chSmartphone2.isChecked()) {
                    chSmartphone2.setChecked(false);
                } else {
                    chSmartphone2.setChecked(true);
                }
                break;
            case R.id.ll_tablet2:
                if (chTablet2.isChecked()) {
                    chTablet2.setChecked(false);
                } else {
                    chTablet2.setChecked(true);
                }
                break;
            case R.id.ll_laptop2:
                if (chLaptop2.isChecked()) {
                    chLaptop2.setChecked(false);
                } else {
                    chLaptop2.setChecked(true);
                }
                break;
            case R.id.ll_pc2:
                if (chPc2.isChecked()) {
                    chPc2.setChecked(false);
                } else {
                    chPc2.setChecked(true);
                }
                break;
            case R.id.ll_futbol1:
                if (chFutbol1.isChecked()) {
                    chFutbol1.setChecked(false);
                } else {
                    chFutbol1.setChecked(true);
                }
                break;
            case R.id.ll_voley1:
                if (chVoley1.isChecked()) {
                    chVoley1.setChecked(false);
                } else {
                    chVoley1.setChecked(true);
                }
                break;
            case R.id.ll_bochas1:
                if (chBochas1.isChecked()) {
                    chBochas1.setChecked(false);
                } else {
                    chBochas1.setChecked(true);
                }
                break;
            case R.id.ll_atletismo1:
                if (chAtletismo1.isChecked()) {
                    chAtletismo1.setChecked(false);
                } else {
                    chAtletismo1.setChecked(true);
                }
                break;

            case R.id.ll_natacion2:
                if (chNatacion2.isChecked()) {
                    chNatacion2.setChecked(false);
                } else {
                    chNatacion2.setChecked(true);
                }
                break;
            case R.id.ll_atletismo2:
                if (chAtletismo2.isChecked()) {
                    chAtletismo2.setChecked(false);
                } else {
                    chAtletismo2.setChecked(true);
                }
                break;
            case R.id.ll_pesas2:
                if (chPesas2.isChecked()) {
                    chPesas2.setChecked(false);
                } else {
                    chPesas2.setChecked(true);
                }
                break;
            case R.id.ll_basquet2:
                if (chBasquet2.isChecked()) {
                    chBasquet2.setChecked(false);
                } else {
                    chBasquet2.setChecked(true);
                }
                break;
            case R.id.btn_interests:
                Intent intent = new Intent(getApplicationContext(), ContactActivity.class);
                save();
                startActivity(intent);
                break;
        }
    }

    private void save(){

        /**Session Manager**/
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());

        /**Otros**/
        o_social_network = etOtrosRedsocial.getText().toString();
        o_pref_sports = etOtrosDeportes.getText().toString();

        if (!etOtrosRedsocial.getText().toString().equals(""))
        {
            registerManager.setOthersSocialNetwork(o_social_network);
        }
        if (!etOtrosDeportes.getText().toString().equals(""))
        {
            registerManager.setOthersPreferencesSports(o_pref_sports);

        }

        registerManager.setSocialNetwork(social_network);
        registerManager.setDevices(device);
        registerManager.setPreferenceDevice(pref_device);
        registerManager.setSports(sports);
        registerManager.setPreferencesSports(pref_sports);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }
}
