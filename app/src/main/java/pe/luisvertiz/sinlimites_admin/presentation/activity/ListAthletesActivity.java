package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.app.SearchManager;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.AthleteMin;
import pe.luisvertiz.sinlimites_admin.presentation.fragment.ListAthletes1Fragment;
import pe.luisvertiz.sinlimites_admin.presentation.fragment.ListAthletes2Fragment;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

public class ListAthletesActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener ,SearchView.OnQueryTextListener{
    private UserPresenter presenter;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    Toolbar toolbar;

    private ListAthletes1Fragment athletes1 = new ListAthletes1Fragment();
    private ListAthletes2Fragment athletes2 = new ListAthletes2Fragment();

    int test = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_athletes);


        //TabLayout
        toolbar = (Toolbar) findViewById(R.id.toolbar_list_athletes);
        showToolbar("Lista de Deportistas",true);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        presenter=new UserPresenter(getApplicationContext(), mSectionsPagerAdapter, athletes1, athletes2);
        presenter.getAll();

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position)
            {
                case 0:
                    return athletes1;

                case 1:
                    return athletes2;
            }
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

    }

   private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchitem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchitem);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        TextView searchText = (TextView)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        searchText.setTextColor(Color.parseColor("#FFFFFF"));
        searchText.setHintTextColor(Color.parseColor("#FFFFFF"));
        searchText.setHint("Busca por nombre de atleta");
        searchView.setOnQueryTextListener(this);


        return true;
    }

    //Test
    private void CargarAthletes2Test(ArrayList<AthleteMin> lista2)
    {
        Toast.makeText(getApplicationContext(),"Test 2: "+ lista2.size(), Toast.LENGTH_SHORT).show();

        Bundle bundle2=new Bundle();
        bundle2.putSerializable("athletes_2", lista2);
        athletes2.setArguments(bundle2);
    }

    private void CargarAthletes1Test(ArrayList<AthleteMin> lista1) {
        Toast.makeText(getApplicationContext(),"Test 1: "+ lista1.size(), Toast.LENGTH_SHORT).show();

        Bundle bundle1=new Bundle();
        bundle1.putSerializable("athletes_1", lista1);
        athletes1.setArguments(bundle1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Toast.makeText(this, "SUBMIT", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        Log.e("ServicesFragment", "onQueryTextChange: " + query);
        presenter.filter(query);
        return false;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return true;
    }

}
