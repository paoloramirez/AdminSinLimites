package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonDetail;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

public class PerfilActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_inicio)
    Toolbar toolbarInicio;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.tv_DatosPersonales)
    TextView tvDatosPersonales;
    @BindView(R.id.tv_DocIdentidad)
    TextView tvDocIdentidad;
    @BindView(R.id.tv_FechNacimiento)
    TextView tvFechNacimiento;
    @BindView(R.id.tv_Direccion)
    TextView tvDireccion;
    @BindView(R.id.layout_VerMas)
    LinearLayout layoutVerMas;
    @BindView(R.id.tv_Deportes)
    TextView tvDeportes;
    @BindView(R.id.tv_MisDeportes)
    TextView tvMisDeportes;
    @BindView(R.id.tv_NomContacto)
    TextView tvNomContacto;
    @BindView(R.id.tv_ApeContacto)
    TextView tvApeContacto;
    @BindView(R.id.tv_DocIdentidadContacto)
    TextView tvDocIdentidadContacto;
    @BindView(R.id.tv_CelContacto)
    TextView tvCelContacto;
    @BindView(R.id.tv_CorreoContacto)
    TextView tvCorreoContacto;
    @BindView(R.id.tv_TipoParentesco)
    TextView tvTipoParentesco;
    @BindView(R.id.iconEditContact)
    ImageView iconEditContact;
    @BindView(R.id.tvNombres)
    TextView tvNombres;


    private UserPresenter presenter;
    private PersonDetail person = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        ButterKnife.bind(this);

        presenter = new UserPresenter(getApplicationContext(), person, tvNombres,
                tvDocIdentidad, tvFechNacimiento, tvDireccion,
                tvMisDeportes,
                tvNomContacto, tvApeContacto, tvDocIdentidadContacto, tvCelContacto, tvCorreoContacto, tvTipoParentesco);

        //Obtener id de SharedPreferences...
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        presenter.getDetail(registerManager.getIdPerson());

        showToolbar("Mi Perfil", true);
    }

    @OnClick({R.id.layout_VerMas, R.id.iconEditContact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layout_VerMas:
                person = presenter.getPerson();
                if (person!=null)
                {
                    Intent intent = new Intent(getApplicationContext(), PersonalDataDetailActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No se ha cargado los datos todavia", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.iconEditContact:
                person = presenter.getPerson();
                if (person!=null)
                {
                    Intent intent = new Intent(getApplicationContext(), EditContactDataActivity.class);
                    intent.putExtra("intent_profile_contact", person);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No se ha cargado los datos todavia", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    //Toolbar
    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarInicio);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }
}
