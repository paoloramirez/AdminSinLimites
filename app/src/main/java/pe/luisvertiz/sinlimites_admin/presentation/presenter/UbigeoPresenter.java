package pe.luisvertiz.sinlimites_admin.presentation.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.List;

import pe.luisvertiz.sinlimites_admin.data.entity.Department;
import pe.luisvertiz.sinlimites_admin.data.entity.District;
import pe.luisvertiz.sinlimites_admin.data.entity.Province;
import pe.luisvertiz.sinlimites_admin.data.repository.remote.ServiceGenerator;
import pe.luisvertiz.sinlimites_admin.data.repository.remote.request.UbigeoRequest;
import pe.luisvertiz.sinlimites_admin.presentation.activity.EditPersonalData1Activity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.Register1Activity;
import pe.luisvertiz.sinlimites_admin.presentation.contract.UbigeoContract;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Luis on 08/02/2018.
 */

public class UbigeoPresenter implements UbigeoContract {
    private Context context;
    private SearchableSpinner spinner;
    private SearchableSpinner spinner2 = null;
    private SearchableSpinner spinner3 = null;

    private int initDep;

    public int getInitDep() {
        return initDep;
    }

    public void setInitDep(int initDep) {
        this.initDep = initDep;
    }

    public int getInitProv() {
        return initProv;
    }

    public void setInitProv(int initProv) {
        this.initProv = initProv;
    }

    private int initProv;


    public UbigeoPresenter(Context context, SearchableSpinner spinner) {
        this.context = context;
        this.spinner=spinner;
    }

    public UbigeoPresenter(Context context, SearchableSpinner spinner,
                           SearchableSpinner spinner2, SearchableSpinner spinner3, int initDep, int initProv) {
        this.context = context;
        this.spinner=spinner;
        this.spinner2=spinner2;
        this.spinner3=spinner3;
        this.initDep=initDep;
        this.initProv=initProv;
    }

    /*
     * EditRegistro1
     */

    public void getDepartmentsInit(final int pos, final int pos2, final int pos3) {
        UbigeoRequest ubigeoRequest= ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<Department>> call=ubigeoRequest.getDepartments();
        call.enqueue(new Callback<List<Department>>() {
            @Override
            public void onResponse(Call<List<Department>> call, Response<List<Department>> response) {

                ArrayAdapter<Department> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner.setAdapter(adapter);
                EditPersonalData1Activity.setDepartments(response.body());
                spinner.setSelection((pos/10000)-1);
                Log.e("TAG","cambio departamento");
                getProvincesInit(pos, pos2, pos3);
                initDep++;
            }

            @Override
            public void onFailure(Call<List<Department>> call, Throwable t) {

            }
        });
    }
    public void getProvincesInit(int idDepartment, final int pos2, final int pos3) {
        UbigeoRequest ubigeoRequest=ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<Province>> call=ubigeoRequest.getProvinces(idDepartment);
        call.enqueue(new Callback<List<Province>>() {
            @Override
            public void onResponse(Call<List<Province>> call, Response<List<Province>> response) {
                ArrayAdapter<Province> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner2.setAdapter(adapter);
                EditPersonalData1Activity.setProvinces(response.body());
                spinner2.setSelection(((pos2%10000)/100)-1);
                Log.e("TAG","cambio provincia");
                initProv++;
                getDistrictsInit(pos2, pos3);

            }

            @Override
            public void onFailure(Call<List<Province>> call, Throwable t) {

            }
        });

    }

    public void getDistrictsInit(int idProvince, final int pos3) {
        UbigeoRequest ubigeoRequest=ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<District>> call=ubigeoRequest.getDistricts(idProvince);
        call.enqueue(new Callback<List<District>>() {
            @Override
            public void onResponse(Call<List<District>> call, Response<List<District>> response) {
                ArrayAdapter<District> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner3.setAdapter(adapter);
                EditPersonalData1Activity.setDistricts(response.body());
                if (pos3 != -1) {
                    spinner3.setSelection(((pos3%10000)%100)-1);
                    Log.e("TAG","cambio distrito pos");
                } else {
                    Log.e("TAG","cambio distrito");
                }
            }

            @Override
            public void onFailure(Call<List<District>> call, Throwable t) {

            }
        });

    }

    /*
     * Registro 1 - EditRegistro1
     */

    @Override
    public void getDepartments() {
        UbigeoRequest ubigeoRequest= ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<Department>> call=ubigeoRequest.getDepartments();
        call.enqueue(new Callback<List<Department>>() {
            @Override
            public void onResponse(Call<List<Department>> call, Response<List<Department>> response) {

                ArrayAdapter<Department> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner.setAdapter(adapter);
                spinner.setSelection(0);
                Log.e("CAMBIO", "se cambio el adaptador departamento");
                Register1Activity.setDepartments(response.body());
                EditPersonalData1Activity.setDepartments(response.body());
            }

            @Override
            public void onFailure(Call<List<Department>> call, Throwable t) {

            }
        });
    }

    @Override
    public void getProvinces(int idDepartment) {
        UbigeoRequest ubigeoRequest=ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<Province>> call=ubigeoRequest.getProvinces(idDepartment);
        call.enqueue(new Callback<List<Province>>() {
            @Override
            public void onResponse(Call<List<Province>> call, Response<List<Province>> response) {
                ArrayAdapter<Province> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner.setAdapter(adapter);
                Register1Activity.setProvinces(response.body());
                EditPersonalData1Activity.setProvinces(response.body());
            }

            @Override
            public void onFailure(Call<List<Province>> call, Throwable t) {

            }
        });

    }

    public void getProvinces(final int idDepartment,final int id) {
        UbigeoRequest ubigeoRequest=ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<Province>> call=ubigeoRequest.getProvinces(idDepartment);
        call.enqueue(new Callback<List<Province>>() {
            @Override
            public void onResponse(Call<List<Province>> call, Response<List<Province>> response) {
                ArrayAdapter<Province> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner.setAdapter(adapter);
                Register1Activity.setProvinces(response.body());
                EditPersonalData1Activity.setProvinces(response.body());
            }

            @Override
            public void onFailure(Call<List<Province>> call, Throwable t) {

            }
        });

    }


    @Override
    public void getDistricts(int idProvince) {
        UbigeoRequest ubigeoRequest=ServiceGenerator.createService(UbigeoRequest.class);
        Call<List<District>> call=ubigeoRequest.getDistricts(idProvince);
        call.enqueue(new Callback<List<District>>() {
            @Override
            public void onResponse(Call<List<District>> call, Response<List<District>> response) {
                ArrayAdapter<District> adapter=new ArrayAdapter(context, android.R.layout.simple_spinner_item, response.body());
                spinner.setAdapter(adapter);
                Register1Activity.setDistricts(response.body());
                EditPersonalData1Activity.setDistricts(response.body());
            }

            @Override
            public void onFailure(Call<List<District>> call, Throwable t) {

            }
        });

    }
}
