package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.et_user_login)
    TextInputEditText etUserLogin;
    @BindView(R.id.et_password_login)
    TextInputEditText etPasswordLogin;
    @BindView(R.id.til_user_login)
    TextInputLayout tilUserLogin;
    @BindView(R.id.til_user_password)
    TextInputLayout tilUserPassword;


    private UserPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

   //     initErrors();


        etUserLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!validateEmail(s.toString())) {
                    tilUserLogin.setError("Correo incorrecto");
                } else {
                    tilUserLogin.setError("");
                }

            }
        });

        etPasswordLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    tilUserPassword.setError("Contraseña no puede estar vacía");
                } else{
                    tilUserPassword.setError("");
                }
            }
        });


    }

    private void initErrors() {
        tilUserLogin.setError("Correo incorrecto");
        tilUserPassword.setError("Contraseña no puede estar vacía");
    }


    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);

    }

    private boolean validateEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @OnClick({R.id.btn_login})
    public void onViewClicked(View view) {
        boolean flag=false;
        switch (view.getId()) {
            case R.id.btn_login:

                if(etPasswordLogin.length()>0){
                    flag=true;
                } else{
                    etPasswordLogin.requestFocus();
                    tilUserPassword.setError("Contraseña incorrecta");
                    flag=false;
                }


                if(validateEmail(etUserLogin.getText().toString())){
                    flag=true;
                } else{
                    etUserLogin.requestFocus();
                    tilUserLogin.setError("Correo incorrecto");
                    flag=false;
                }


                if(flag){
                    presenter=new UserPresenter(getApplicationContext());
                    presenter.login(etUserLogin.getText().toString(), etPasswordLogin.getText().toString());
                } else{
                    Toast.makeText(getApplicationContext(), "No puede ingresar", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }
}
