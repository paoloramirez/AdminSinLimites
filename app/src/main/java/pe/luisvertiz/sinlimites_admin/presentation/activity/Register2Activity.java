package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pe.luisvertiz.sinlimites_admin.BuildConfig;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.util.camera.UploadImageInterface;
import pe.luisvertiz.sinlimites_admin.util.camera.UploadObject;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;



public class Register2Activity extends AppCompatActivity {

    @BindView(R.id.toolbar_register2)
    Toolbar toolbarRegister2;
    @BindView(R.id.iv_info)
    ImageView ivInfo;
    @BindView(R.id.imgPhotoAthlete)
    ImageView imgPhotoAthlete;
    @BindView(R.id.layoutPhotoAthlete)
    RelativeLayout layoutPhotoAthlete;
    @BindView(R.id.btn_register2)
    AppCompatButton btnRegister2;
    @BindView(R.id.ll_visual)
    LinearLayout llVisual;
    @BindView(R.id.ll_intelectual)
    LinearLayout llIntelectual;
    @BindView(R.id.ll_fisica)
    LinearLayout llFisica;
    @BindView(R.id.ll_auditiva)
    LinearLayout llAuditiva;
    @BindView(R.id.ch_visual)
    AppCompatCheckBox chVisual;
    @BindView(R.id.ch_fisica)
    AppCompatCheckBox chFisica;
    @BindView(R.id.ch_auditiva)
    AppCompatCheckBox chAuditiva;
    @BindView(R.id.ch_intelectual)
    AppCompatCheckBox chIntelectual;
    @BindView(R.id.tv_error_discap)
    TextView tvErrorDiscap;
    @BindView(R.id.et_history)
    EditText etHistory;

    boolean flagDiscap;
    @BindView(R.id.et_conadis)
    EditText etConadis;
    @BindView(R.id.ch_publico)
    AppCompatCheckBox chPublico;
    @BindView(R.id.ll_publico)
    LinearLayout llPublico;
    @BindView(R.id.ch_privado)
    AppCompatCheckBox chPrivado;
    @BindView(R.id.ll_privado)
    LinearLayout llPrivado;

    boolean flagChVisual, flagChIntelectual, flagChFisica, flagChAuditiva;
    @BindView(R.id.iv_cam)
    ImageView ivCam;

    private static String APP_DIRECTORY = "SinLimites/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "Imagenes";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private static final int READ_REQUEST_CODE = 400;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String SERVER_PATH = "http://rockenwebperu.com/";

    private String mPath;
    private Uri uri_p;
    Long timestamp;
    int disability = 0;
    int seguro = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);
        ButterKnife.bind(this);

        initFlags();

        showToolbar("Datos Personales", true);

        chVisual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChVisual = true;
                    tvErrorDiscap.setText("");
                    disability+=1;
                } else {
                    disability-=1;
                    if (chAuditiva.isChecked() || chFisica.isChecked() || chIntelectual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChVisual = false;

                }
            }
        });

        chIntelectual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChIntelectual = true;
                    tvErrorDiscap.setText("");
                    disability+=2;
                } else {
                    disability-=2;
                    if (chAuditiva.isChecked() || chFisica.isChecked() || chVisual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChIntelectual = false;
                }
            }
        });

        chAuditiva.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChAuditiva = true;
                    disability+=8;
                } else {
                    disability-=8;
                    if (chVisual.isChecked() || chFisica.isChecked() || chIntelectual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChAuditiva = false;
                }
            }
        });

        chFisica.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChFisica = true;
                    disability+=4;
                } else {
                    disability-=4;
                    if (chVisual.isChecked() || chAuditiva.isChecked() || chIntelectual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChFisica = false;
                }

            }
        });

        chPublico.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    seguro+=1;
                } else {
                    seguro-=1;
                }

            }
        });

        chPrivado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    seguro+=2;
                } else {
                    seguro-=2;
                }

            }
        });


    }

    private void initFlags() {
        flagDiscap = false;
    }

    private void initErrors() {
        tvErrorDiscap.setText("Seleccione su discapacidad");
    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }


    @OnClick({R.id.iv_info, R.id.ll_visual, R.id.ll_intelectual, R.id.ll_fisica, R.id.ll_auditiva, R.id.layoutPhotoAthlete, R.id.btn_register2, R.id.ll_publico, R.id.ll_privado, R.id.iv_cam})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.iv_info:
                showDialogConadis();
                break;
            case R.id.ll_visual:
                if (chVisual.isChecked()) {
                    chVisual.setChecked(false);
                } else {
                    chVisual.setChecked(true);
                }

                break;
            case R.id.ll_intelectual:
                if (chIntelectual.isChecked()) {
                    chIntelectual.setChecked(false);
                } else {
                    chIntelectual.setChecked(true);
                }
                break;
            case R.id.ll_fisica:
                if (chFisica.isChecked()) {
                    chFisica.setChecked(false);
                } else {
                    chFisica.setChecked(true);
                }
                break;
            case R.id.ll_auditiva:
                if (chAuditiva.isChecked()) {
                    chAuditiva.setChecked(false);
                } else {
                    chAuditiva.setChecked(true);
                }
                break;


            case R.id.layoutPhotoAthlete:
                break;
            case R.id.btn_register2:

                if (chFisica.isChecked() || chAuditiva.isChecked() || chIntelectual.isChecked() || chVisual.isChecked()) {
                    saveData();
                    launchActivity(getApplicationContext(), InterestsActivity.class);
                } else {
                    tvErrorDiscap.setText("Seleccione su discapacidad");
                    Toast.makeText(getApplicationContext(), "SELECCIONE SU DISCAPACIDAD", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ll_privado:
                break;

            case R.id.ll_publico:
                break;

            case R.id.iv_cam:
                showCamera();
        }

        if (chFisica.isChecked() || chVisual.isChecked() || chIntelectual.isChecked() || chAuditiva.isChecked()) {
            tvErrorDiscap.setText("");
        } else {

            tvErrorDiscap.setText("Seleccione su discapacidad");
        }
    }

    private void saveData() {
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        registerManager.setCodeConadis(etConadis.getText().toString());
        registerManager.setDisability(disability);
        registerManager.setHistory(etHistory.getText().toString());
        registerManager.setTypeSafe(seguro);

    }

    private void showCamera() {
        if (myRequestStoragePermissions()){
            showOptions();
            Log.d("TAG1","entro permissions");
        } else{
            Log.d("TAG1","no entro permissions");
        }


    }

    private void showDialogConadis() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.dialog_conadis, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    private void sendPhoto() {

        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            String filePath = getRealPathFromURIPath(uri_p, this);
            File file = new File(filePath);
            Log.d(TAG, "Filename " + file.getName());
            //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(SERVER_PATH)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
            Call<UploadObject> fileUpload = uploadImage.uploadFile(fileToUpload, filename);
            fileUpload.enqueue(new Callback<UploadObject>() {
                @Override
                public void onResponse(Call<UploadObject> call, Response<UploadObject> response) {
                    Toast.makeText(Register2Activity.this, "Response " + response.raw().message(), Toast.LENGTH_LONG).show();
                    Toast.makeText(Register2Activity.this, "Success " + response.body().getSuccess(), Toast.LENGTH_LONG).show();

                }

                @Override
                public void onFailure(Call<UploadObject> call, Throwable t) {
                    Log.d(TAG, "Error " + t.getMessage());
                }
            });
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_file), READ_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }


    private boolean myRequestStoragePermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if ((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED))

            return true;

        if ((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) ||
                shouldShowRequestPermissionRationale(CAMERA)) {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        }

        return false;
    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if (!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if (isDirectoryCreated) {
            timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(mPath);
            Uri photoURI = FileProvider.getUriForFile(Register2Activity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    newFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    private void showOptions() {
        final CharSequence[] option = {"Tomar foto", "Elegir de galería", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(Register2Activity.this);

        builder.setTitle("Elige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (option[i] == "Tomar foto") {
                    openCamera();
                } else if (option[i] == "Elegir de galería") {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_PICTURE);
                } else {
                    dialogInterface.dismiss();
                }
            }
        });

        builder.show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState);
        outState.putString("file path", mPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPath = savedInstanceState.getString("file path");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,
                            new String[]{mPath}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> Uri = " + uri);
                                    uri_p = uri;
                                    iniciarCrop(uri_p);
                                }
                            });
                    break;

                case SELECT_PICTURE:
                    uri_p = data.getData();
                    iniciarCrop(uri_p);
                    break;

                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    uri_p = result.getUri();
                    mPath = getRealPathFromURIPath(uri_p, Register2Activity.this);
                    imgPhotoAthlete.setImageURI(uri_p);
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(Register2Activity.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                showOptions();
            } else {
                showExplanation();
            }
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Register2Activity.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Se necesitan permisos para realizar las acciones");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);

                startActivity(intent);
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });

        builder.show();
    }

    private static Bitmap loadAndResizeBitmap(String fileName, int width, int height) {
        // First we get the the dimensions of the file on disk
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);

        // Next we calculate the ratio that we need to resize the image by
        // to fit the requested dimensions.
        int outHeight = options.outHeight;
        int outWidth = options.outWidth;
        int inSampleSize = 1;

        if (outHeight > height || outWidth > width) {
            inSampleSize = outWidth > outHeight
                    ? outHeight / height
                    : outWidth / width;
        }

        // Now we will load the image and have BitmapFactory resize it for us.
        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        Bitmap resizedBitmap = BitmapFactory.decodeFile(fileName, options);

        return resizedBitmap;
    }

    private Bitmap recortar(Bitmap bmp_tmp, int x, int y, int width, int height) {
        int a = bmp_tmp.getWidth();
        int b = bmp_tmp.getHeight();
        Log.d("TEST", "A: " + Integer.toString(a) + "\nB: " + Integer.toString(b));
        Bitmap bmp = Bitmap.createBitmap(bmp_tmp, x, y, a, b);

        try {
            FileOutputStream fos = new FileOutputStream(mPath);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();


        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }

        return bmp;
    }

    private void comprimir() {
        Bitmap bitmap = loadAndResizeBitmap(mPath, 300, 300);
        // BitmapFactory.decodeFile(mPath);
        recortar(bitmap, 0, 0, 300, 300);
    }

    private void iniciarCrop(Uri imageUri) {
        Rect cuadro = new Rect(300, 300, 300, 300);

        CropImage.activity(imageUri)
                .setBackgroundColor(1)
                .setMinCropResultSize(100, 100)
                .setInitialCropWindowRectangle(cuadro)
                .setActivityTitle("Editar foto")
                .setCropMenuCropButtonTitle("Guardar")
                .setAllowRotation(true)
                .setBorderLineColor(Color.RED)
                .start(this);
    }

    private void cambiarCrop(Uri uri) {
        //cropImageView.setImageUriAsync(uri);
    }


    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }


    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarRegister2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }


}











