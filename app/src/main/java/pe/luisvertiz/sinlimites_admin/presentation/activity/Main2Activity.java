package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.toolbar_menu)
    Toolbar toolbarMenu;
    @BindView(R.id.cv_add_athlete)
    CardView cvAddAthlete;
    @BindView(R.id.cv_list_athletes)
    CardView cvListAthletes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.cv_add_athlete, R.id.cv_list_athletes})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_add_athlete:
                launchActivity(getApplicationContext(), Register1Activity.class);
                break;
            case R.id.cv_list_athletes:
                launchActivity(getApplicationContext(), ListAthletesActivity.class);
                break;
        }
    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);
    }
}
