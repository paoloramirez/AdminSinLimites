package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Validator;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.Department;
import pe.luisvertiz.sinlimites_admin.data.entity.District;
import pe.luisvertiz.sinlimites_admin.data.entity.Province;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UbigeoPresenter;

public class Register1Activity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    @BindView(R.id.toolbar_register1)
    Toolbar toolbarRegister1;

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_lastname)
    EditText etLastname;

    @BindView(R.id.et_birthday)
    EditText etBirthday;


    @BindView(R.id.sp_genero)
    AppCompatSpinner spGenero;
    @BindView(R.id.sp_tipo_doc)
    AppCompatSpinner spTipoDoc;

    @BindView(R.id.et_number_document)
    EditText etNumberDocument;

    @BindView(R.id.et_telephone)
    EditText etTelephone;
    @BindView(R.id.sp_departments)
    SearchableSpinner spDepartments;
    @BindView(R.id.sp_provinces)
    SearchableSpinner spProvinces;

    @BindView(R.id.sp_districts)
    SearchableSpinner spDistricts;

    @BindView(R.id.et_address)
    EditText etAddress;


    @BindView(R.id.iv_voice)
    ImageView ivVoice;
    @BindView(R.id.btn_register1)
    AppCompatButton btnRegister1;

    int counterGenero = 0;
    int counterTipoDoc = 0;
    int counterDistrito = 0;
    Validator validator;
    @BindView(R.id.tv_error_name)
    TextView tvErrorName;
    @BindView(R.id.tv_error_lastname)
    TextView tvErrorLastname;
    @BindView(R.id.tv_error_birthday)
    TextView tvErrorBirthday;
    @BindView(R.id.tv_error_number_doc)
    TextView tvErrorNumberDoc;
    @BindView(R.id.tv_error_telephone)
    TextView tvErrorTelephone;
    @BindView(R.id.tv_error_address)
    TextView tvErrorAddress;

    boolean flagName, flagLastName, flagBirthday, flagNumberDocument, flagTelephone, flagAddress;
    @BindView(R.id.btn_calendar)
    AppCompatButton btnCalendar;

    private UbigeoPresenter departmentsPresenter, provincePresenter, districtPresenter;
    private static List<Department> listDepartments;
    private static List<Province> listProvinces;
    private static List<District> listDistrict;

    int codeDistrito = 0;

    private TextToSpeech textToSpeech;
    private static final int RECOGNIZE_SPEECH_ACTIVITY = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register1);
        ButterKnife.bind(this);

        textToSpeech = new TextToSpeech(this, this);
        textToSpeech.setLanguage(new Locale("spa", "ESP"));


        showToolbar("Datos Personales", true);

        initFlags();

        etName.requestFocus();
        //  disableAllViews();
        hideKeyboard();

        initSpinnerGenero();
        initSpinnerTipoDoc();
        initSpinnerDepartments();

        //     initErrors();

        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etName.length() == 0) {
                        tvErrorName.setText("Este campo es obligatorio");
                    } else {
                        etLastname.requestFocus();
                    }
                }
                return true;
            }
        });

        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    tvErrorName.setText("Este campo es obligatorio");
                    flagName = false;
                } else {
                    tvErrorName.setText("");
                    flagName = true;
                }
            }
        });


        etLastname.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etLastname.length() == 0) {
                        tvErrorLastname.setText("Este campo es obligatorio");
                    } else {
                        etBirthday.requestFocus();
                        hideKeyboard();
                    }
                }
                return false;
            }
        });

        etLastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagLastName = false;
                    tvErrorLastname.setText("Este campo es obligatorio");
                } else {
                    flagLastName = true;
                    tvErrorLastname.setText("");
                }
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                showPickerDate();
            }
        });

        etBirthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard();
                showPickerDate();
                return true;
            }
        });



    /*    validator = new Validator(this);
        validator.setValidationListener(this); */

        spGenero.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideKeyboard();
                }
            }
        });


        spGenero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    // selecciono masculino
                    case 0:
                        break;

                    // selecciono femenino
                    case 1:
                        break;
                }

                if (counterGenero > 0) {
                    spTipoDoc.requestFocus();
                }
                counterGenero++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        spTipoDoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (counterTipoDoc > 0) {
                    etNumberDocument.requestFocus();
                    showKeyboard();
                    switch (position) {
                        // selecciono masculino
                        case 0:
                            etNumberDocument.setText("");

                            etNumberDocument.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                            break;

                        // selecciono femenino
                        case 1:
                            etNumberDocument.setText("");
                       /*     if (etNumberDocument.length() == 0) {
                                tvErrorNumberDoc.setText("Este campo es obligatorio");
                                flagNumberDocument=false;
                            } else if (etNumberDocument.length() == 9) {
                                tvErrorNumberDoc.setText("");
                                flagNumberDocument=true;
                            } else {
                                flagNumberDocument=false;
                                tvErrorNumberDoc.setText("Es necesario 9 digitos");
                            } */
                            etNumberDocument.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                            break;

                    }
                } else {
                    counterTipoDoc++;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etNumberDocument.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etNumberDocument.length() == 0) {
                        tvErrorLastname.setText("Este campo es obligatorio");
                    }
                    etTelephone.requestFocus();

                }
                return true;
            }
        });

        etNumberDocument.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagNumberDocument = false;
                    tvErrorNumberDoc.setText("Este campo es obligatorio");
                } else {
                    if (spTipoDoc.getSelectedItemPosition() == 0) {
                        if (s.length() != 8) {
                            flagNumberDocument = false;
                            tvErrorNumberDoc.setText("Es necesario 8 digitos");
                        } else {
                            flagNumberDocument = true;
                            tvErrorNumberDoc.setText("");
                        }


                    } else if (spTipoDoc.getSelectedItemPosition() == 1) {
                        if (s.length() != 9) {
                            flagNumberDocument = false;
                            tvErrorNumberDoc.setText("Es necesario 9 digitos");
                        } else {
                            flagNumberDocument = true;
                            tvErrorNumberDoc.setText("");
                        }

                    }

                }


            }
        });

        etTelephone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etTelephone.length() == 0) {
                        tvErrorTelephone.setText("OPCIONAL");
                    } else {
                        spDepartments.requestFocus();
                        hideKeyboard();
                    }


                }
                return true;
            }
        });

        etTelephone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagTelephone = false;
                    tvErrorTelephone.setText("OPCIONAL");
                } else if (s.length() < 7 || s.length() > 9 || s.length() == 8) {
                    flagTelephone = false;
                    tvErrorTelephone.setText("Ingrese un telefono correcto");
                } else {
                    flagTelephone = true;
                    tvErrorTelephone.setText("");
                }

            }
        });

        spDepartments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (listDepartments != null) {
                    Department department = listDepartments.get(position);
                    //   Toast.makeText(getApplicationContext(), department.getId()+"", Toast.LENGTH_SHORT).show();

                    provincePresenter = new UbigeoPresenter(getApplicationContext(), spProvinces);
                    provincePresenter.getProvinces(department.getId());
                    spProvinces.requestFocus();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spProvinces.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (listProvinces != null) {
                    Province province = listProvinces.get(position);
                    //   Toast.makeText(getApplicationContext(), department.getId()+"", Toast.LENGTH_SHORT).show();

                    districtPresenter = new UbigeoPresenter(getApplicationContext(), spDistricts);
                    districtPresenter.getDistricts(province.getId());
                    spDistricts.requestFocus();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spDistricts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                District district = listDistrict.get(position);
                codeDistrito = district.getId();

                if (counterDistrito != 0) {
                    etAddress.requestFocus();
                    showKeyboard();
                }
                counterDistrito += 1;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPickerDate();
                hideKeyboard();
            }
        });

        etAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard();
                    if (etAddress.length() == 0) {
                        flagAddress = false;
                        tvErrorAddress.setText("Este campo es obligatorio");
                    }
                }
                return true;
            }
        });

        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagAddress = false;
                    tvErrorAddress.setText("Este campo es obligatorio");
                } else {
                    flagAddress = true;
                    tvErrorAddress.setText("");
                }

            }
        });


    }

    private void initSpinnerDepartments() {
        departmentsPresenter = new UbigeoPresenter(getApplicationContext(), spDepartments);
        departmentsPresenter.getDepartments();

    }

    private void initFlags() {
        flagName = false;
        flagLastName = false;
        flagBirthday = false;
        flagNumberDocument = false;
        flagTelephone = false;
        flagAddress = false;
    }

    private void initErrors() {
        tvErrorName.setText("OBLIGATORIO");
        tvErrorLastname.setText("OBLIGATORIO");
        tvErrorBirthday.setText("OBLIGATORIO");
        tvErrorAddress.setText("OBLIGATORIO");
        tvErrorNumberDoc.setText("OBLIGATORIO");
        tvErrorTelephone.setText("OPCIONAL");
    }

    private void initSpinnerTipoDoc() {
        String[] tipoDocs = new String[]{"DNI", "Carné de Extranjería"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tipoDocs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoDoc.setAdapter(adapter);


    }

    private void initSpinnerGenero() {
        String[] generos = new String[]{"Masculino", "Femenino"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, generos);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGenero.setAdapter(adapter);

    }


    private void hideKeyboard() {
        InputMethodManager imm = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
        imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

    }


    public static void setDepartments(List<Department> lista) {
        listDepartments = new ArrayList<>();
        listDepartments = lista;
    }

    public static void setProvinces(List<Province> lista) {
        listProvinces = new ArrayList<>();
        listProvinces = lista;
    }

    public static void setDistricts(List<District> lista) {
        listDistrict = new ArrayList<>();
        listDistrict = lista;
    }


    private void showPickerDate() {
        final Calendar c = Calendar.getInstance();
        int currentDay = c.get(Calendar.DAY_OF_MONTH);
        int currentMonth = c.get(Calendar.MONTH);
        int currentYear = c.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //  hideKeyboard();
                etBirthday.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                tvErrorBirthday.setText("");
                spGenero.requestFocus();
                hideKeyboard();
                flagBirthday = true;
            }
        }, currentYear, currentMonth, currentDay);
        datePickerDialog.show();
    }


    private void validateInputs() {
        boolean flag = false;

        boolean flagNameT = false, flagLastNameT = false, flagAddressT = false, flagBirthdayT = false, flagNumberDocumentT = false, flagTelephoneT = false;


        if (!flagName) {
            etName.requestFocus();
            tvErrorName.setText("Este campo es obligatorio");
            flagNameT = false;
        } else {
            flagNameT = true;
        }

        if (!flagLastName) {
            etLastname.requestFocus();
            tvErrorLastname.setText("Este campo es obligatorio");
            flagLastNameT = false;
        } else {
            flagLastNameT = true;
        }

        if (!flagAddress) {
            etAddress.requestFocus();
            tvErrorAddress.setText("Este campo es obligatorio");
            flagAddressT = false;
        } else {
            flagAddressT = true;
        }


        if (!flagBirthday) {
            etBirthday.requestFocus();
            tvErrorBirthday.setText("Este campo es obligatorio");
            flagBirthdayT = false;
        } else {
            flagBirthdayT = true;
        }

        if (!flagNumberDocument) {
            etNumberDocument.requestFocus();
            tvErrorNumberDoc.setText("Este campo es obligatorio");
            flagNumberDocumentT = false;
        } else {
            flagNumberDocumentT = true;
        }

        if (!flagTelephone) {
            tvErrorTelephone.requestFocus();
        } else {
            flagTelephoneT = true;
        }


        if (flagNameT && flagLastNameT && flagAddressT && flagBirthdayT && flagNumberDocumentT && flagTelephoneT) {
            saveData();
            launchActivity(getApplicationContext(), Register2Activity.class);
        } else {
            Toast.makeText(getApplicationContext(), "Faltan completar datos", Toast.LENGTH_SHORT).show();
        }


    }

    private void saveData() {
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        registerManager.setName(etName.getText().toString());
        registerManager.setLastName(etLastname.getText().toString());
        registerManager.setBirthday(etBirthday.getText().toString());
        registerManager.setGenero(spGenero.getSelectedItem().toString());
        registerManager.setCodeTipoCod(spTipoDoc.getSelectedItemPosition() + 1);
        registerManager.setNumberDocument(etNumberDocument.getText().toString());
        registerManager.setTelephone(etTelephone.getText().toString());
        registerManager.setCodeDistrict(codeDistrito);
        registerManager.setAddress(etAddress.getText().toString());

    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);
    }

    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarRegister1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.LANG_MISSING_DATA | status == TextToSpeech.LANG_NOT_SUPPORTED) {
            Toast.makeText(this, "ERROR LANG_MISSING_DATA | LANG_NOT_SUPPORTED", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick({R.id.iv_voice, R.id.btn_register1, R.id.btn_calendar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_voice:
                enableVoice();
                break;
            case R.id.btn_register1:
                validateInputs();
                ;
                break;

            case R.id.btn_calendar:
                showPickerDate();
        }
    }

    private void enableVoice() {
        Intent intentActionRecognizeSpeech = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        // Configura el Lenguaje (Español-Perú)
        intentActionRecognizeSpeech.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-PE");
        try {
            startActivityForResult(intentActionRecognizeSpeech,
                    RECOGNIZE_SPEECH_ACTIVITY);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Tú dispositivo no soporta el reconocimiento por voz",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RECOGNIZE_SPEECH_ACTIVITY:
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> speech = data
                            .getStringArrayListExtra(RecognizerIntent.
                                    EXTRA_RESULTS);
                    String strSpeech2Text = speech.get(0);
                    etAddress.setText(strSpeech2Text);
                }
                break;
            default:
                break;
        }
    }



}
