package pe.luisvertiz.sinlimites_admin.presentation.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.AthleteMin;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.activity.PerfilActivity;


/**
 * Created by Capsula01 on 1/02/2018.
 */

public class AthletesAdapter extends RecyclerView.Adapter<AthletesAdapter.AthletesViewHolder> {
    private ArrayList<AthleteMin> list;
    private Context context;
    private String query = "";
    private int fragment = 0;

    public AthletesAdapter(ArrayList<AthleteMin> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public AthletesAdapter(ArrayList<AthleteMin> list, Context context, int fragment) {
        this.list = list;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public AthletesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_athlete, parent, false);
        AthletesViewHolder holder = new AthletesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final AthletesViewHolder holder, int position) {
        final AthleteMin athleteMin =list.get(position);
        holder.tvNames.setText(athleteMin.getPersonFirstName()+" "+ athleteMin.getPersonLastName());

        String name=(athleteMin.getPersonFirstName()+" "+ athleteMin.getPersonLastName()).toLowerCase();

        if (name.contains(query)) {
            int startPos = name.indexOf(query);
            int endPos = startPos + query.length();

            Spannable spanString = Spannable.Factory.getInstance().newSpannable(holder.tvNames.getText());
            spanString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorSearch)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            holder.tvNames.setText(spanString);
        }

        switch (fragment)
        {
            case 1:
                holder.tvSports.setText(athleteMin.getSportstoString());
                break;

            case 2:
                holder.tvSports.setText("");
                holder.tvLabelSports.setText("");
                break;
        }

        holder.ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PerfilActivity.class);
                RegisterManager registerManager = RegisterManager.getInstance(context);
                registerManager.setIdPerson(athleteMin.getIdPerson());
                context.startActivity(intent);
            }
        });

        holder.tvNames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PerfilActivity.class);
                RegisterManager registerManager = RegisterManager.getInstance(context);
                registerManager.setIdPerson(athleteMin.getIdPerson());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class AthletesViewHolder extends RecyclerView.ViewHolder{
        private ImageView ivPhoto;
        private TextView tvNames;
        private TextView tvLabelSports, tvSports;

        public AthletesViewHolder(View itemView) {
            super(itemView);
            ivPhoto=itemView.findViewById(R.id.iv_photo);
            tvNames=itemView.findViewById(R.id.tv_names);
            tvSports = itemView.findViewById(R.id.tv_sports);
            tvLabelSports = itemView.findViewById(R.id.tv_label_sports);
        }
    }

    public void filter(ArrayList data, String query){
        this.list = data;
        this.query = query;
        notifyDataSetChanged();
    }
}
