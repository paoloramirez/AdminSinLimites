package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.Contact;
import pe.luisvertiz.sinlimites_admin.data.entity.ContactUpdate;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonDetail;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;


public class EditContactDataActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_actualizar_datos_contacto)
    Toolbar toolbarActualizarDatosContacto;
    @BindView(R.id.et_name_contact)
    EditText etNameContact;
    @BindView(R.id.et_lastname_contact)
    EditText etLastnameContact;
    @BindView(R.id.et_number_document_contact)
    EditText etNumberDocumentContact;
    @BindView(R.id.et_telephone_contact)
    EditText etTelephoneContact;
    @BindView(R.id.et_email_contact)
    EditText etEmailContact;
    @BindView(R.id.rbn_padre)
    AppCompatRadioButton rbnPadre;
    @BindView(R.id.rbn_tutor)
    AppCompatRadioButton rbnTutor;
    @BindView(R.id.ll_tutor)
    LinearLayout llTutor;
    @BindView(R.id.rbn_otro)
    AppCompatRadioButton rbnOtro;
    @BindView(R.id.ll_otro)
    LinearLayout llOtro;
    @BindView(R.id.ll_padre)
    LinearLayout llPadre;
    @BindView(R.id.et_otro_contact)
    EditText etOtroContact;
    @BindView(R.id.btn_save)
    AppCompatButton btnSave;

    boolean flagName, flagLastName, flagNumberDocument, flagTelephone, flagEmail, flagTipoParentesco, flagOtroNombre;
    @BindView(R.id.sp_tipo_doc_contact)
    AppCompatSpinner spTipoDocContact;
    @BindView(R.id.tv_error_name_contact)
    TextView tvErrorNameContact;
    @BindView(R.id.tv_error_lastname_contact)
    TextView tvErrorLastnameContact;
    @BindView(R.id.tv_error_number_doc_contact)
    TextView tvErrorNumberDocContact;
    @BindView(R.id.tv_error_telephone_contact)
    TextView tvErrorTelephoneContact;
    @BindView(R.id.tv_error_email_contact)
    TextView tvErrorEmailContact;
    @BindView(R.id.tv_error_parentesco_contact)
    TextView tvErrorParentescoContact;
    @BindView(R.id.tv_error_nom_contact)
    TextView tvErrorNomContact;

    int relationship = 0;

    private PersonDetail person;
    private UserPresenter presenter;
    private int counterTipoDoc = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact_data);
        ButterKnife.bind(this);
        etOtroContact.setEnabled(false);

        showToolbar("Actualizar Contacto", true);

        initFlags();
        //Llenar datos iniciales a editar
        Intent intent = getIntent();
        person = (PersonDetail)intent.getSerializableExtra("intent_profile_contact");
        initSpinnerTipoDoc();
        rellenarDatos();

        etNameContact.requestFocus();

        etNameContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etNameContact.length() == 0) {
                        tvErrorNameContact.setText("Este campo es obligatorio");
                    } else {
                        etLastnameContact.requestFocus();
                    }
                }
                return true;
            }
        });

        etNameContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    tvErrorNameContact.setText("Este campo es obligatorio");
                    flagName = false;
                } else {
                    tvErrorNameContact.setText("");
                    flagName = true;
                }

            }
        });

        spTipoDocContact.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (counterTipoDoc > 0) {
                    etNumberDocumentContact.requestFocus();
                    showKeyboard();
                    switch (position) {
                        // selecciono masculino
                        case 0:
                            etNumberDocumentContact.setText("");
                            etNumberDocumentContact.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                            break;

                        // selecciono femenino
                        case 1:
                            etNumberDocumentContact.setText("");
                            etNumberDocumentContact.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                            break;

                    }
                } else {
                    counterTipoDoc++;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etLastnameContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etLastnameContact.length() == 0) {
                        tvErrorLastnameContact.setText("Este campo es obligatorio");
                    } else {
                        etNumberDocumentContact.requestFocus();
                    }
                }
                return true;
            }
        });

        etLastnameContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    tvErrorLastnameContact.setText("Este campo es obligatorio");
                    flagLastName = false;
                } else {
                    tvErrorLastnameContact.setText("");
                    flagLastName = true;
                }

            }
        });

        etNumberDocumentContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etNumberDocumentContact.length() == 0) {
                        tvErrorNumberDocContact.setText("Este campo es obligatorio");
                    }
                    etTelephoneContact.requestFocus();

                }
                return true;
            }
        });

        etNumberDocumentContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagNumberDocument = false;
                    tvErrorNumberDocContact.setText("Este campo es obligatorio");
                } else {
                    if (spTipoDocContact.getSelectedItemPosition() == 0) {
                        if (s.length() != 8) {
                            flagNumberDocument = false;
                            tvErrorNumberDocContact.setText("Es necesario 8 digitos");
                        } else {
                            flagNumberDocument = true;
                            tvErrorNumberDocContact.setText("");
                        }


                    } else if (spTipoDocContact.getSelectedItemPosition() == 1) {
                        if (s.length() != 9) {
                            flagNumberDocument = false;
                            tvErrorNumberDocContact.setText("Es necesario 9 digitos");
                        } else {
                            flagNumberDocument = true;
                            tvErrorNumberDocContact.setText("");
                        }

                    }

                }
            }

        });

        etTelephoneContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etTelephoneContact.length() == 0) {
                        tvErrorTelephoneContact.setText("Este campo es obligatorio");
                    } else {
                        etEmailContact.requestFocus();
                    }


                }
                return true;
            }
        });

        etTelephoneContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagTelephone = false;
                    tvErrorTelephoneContact.setText("Este campo es obligatorio");
                } else if (s.length() != 9) {
                    flagTelephone = false;
                    tvErrorTelephoneContact.setText("Es necesario 9 digitos");
                } else {
                    flagTelephone = true;
                    tvErrorTelephoneContact.setText("");
                }
            }
        });

        etEmailContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                 /*   if (etTelephoneContact.length() == 0) {
                        tvErrorTelephoneContact.setText("Este campo es obligatorio");
                    } else { */
                    rbnPadre.requestFocus();
                    hideKeyboard();
                    //   }
                }
                return true;
            }
        });

        etEmailContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0) {
                    if (!validateEmail(s.toString())) {
                        tvErrorEmailContact.setText("Correo incorrecto");
                        flagEmail = false;
                    } else {
                        tvErrorEmailContact.setText("");
                        flagEmail = true;
                    }
                }

            }
        });


        rbnPadre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relationship = 1;
                    rbnTutor.setChecked(false);
                    rbnOtro.setChecked(false);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    tvErrorParentescoContact.setText("");
                    //       flagOtroNombre=false;
                    flagTipoParentesco=true;
                    flagOtroNombre=true;
                } else{
                    tvErrorNomContact.setText("");
                    hideKeyboard();
                }

            }
        });

        rbnTutor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relationship = 2;
                    rbnPadre.setChecked(false);
                    rbnOtro.setChecked(false);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    tvErrorParentescoContact.setText("");
                    //     flagOtroNombre=false;
                    flagTipoParentesco=true;
                    flagOtroNombre=true;
                } else{
                    tvErrorNomContact.setText("");
                    hideKeyboard();
                }

            }
        });

        rbnOtro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relationship = 3;
                    rbnPadre.setChecked(false);
                    rbnTutor.setChecked(false);
                    etOtroContact.setEnabled(true);
                    etOtroContact.setText("");
                    etOtroContact.requestFocus();
                    tvErrorParentescoContact.setText("");
                    flagTipoParentesco=true;
                    showKeyboard();
                    tvErrorNomContact.setText("Nombre incorrecto");

                } else {
                    tvErrorNomContact.setText("");
                    hideKeyboard();
                }
            }
        });

        etOtroContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (etOtroContact.length() == 0) {
                        tvErrorNomContact.setText("Este campo no puede estar vacío");
                    } else {
                        tvErrorNomContact.setText("");
                    }

                }
                return true;
            }
        });

        etOtroContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(etOtroContact.isEnabled())
                {
                    if(s.length()==0){
                        flagOtroNombre=false;
                        tvErrorNomContact.setText("Nombre incorrecto");
                    } else{
                        flagOtroNombre=true;
                        tvErrorNomContact.setText("");
                    }
                }

            }
        });

    }

    private void rellenarDatos() {

        try {
            Contact contact = person.getContact();

            //Datos
            etNameContact.setText(contact.getContactFirstName());
            flagName=true;
            etLastnameContact.setText(contact.getContactLastName());
            flagLastName=true;
            spTipoDocContact.setSelection(contact.getIdDocumentType()-1);
            switch (contact.getIdDocumentType())
            {
                case 1:
                    etNumberDocumentContact.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                    break;
                case 2:
                    etNumberDocumentContact.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9)});
                    break;
            }
            flagNumberDocument=true;
            etNumberDocumentContact.setText(contact.getContactDocumentNumber());
            etTelephoneContact.setText(contact.getContactPhone());
            flagTelephone=true;
            if (!contact.getContactEmail().equals(""))
            {
                etEmailContact.setText(contact.getContactEmail());
                flagEmail=true;
            }

            flagOtroNombre=true;
            switch (contact.getIdRelationship())
            {
                case 1:
                    rbnPadre.setChecked(true);
                    tvErrorParentescoContact.setText("");
                    flagTipoParentesco=true;
                    relationship = 1;
                    break;
                case 2:
                    rbnTutor.setChecked(true);
                    tvErrorParentescoContact.setText("");
                    flagTipoParentesco=true;
                    relationship = 2;
                    break;
                case 3:
                    rbnOtro.setChecked(true);
                    relationship = 3;
                    etOtroContact.setEnabled(true);
                    tvErrorParentescoContact.setText("");
                    flagTipoParentesco=true;
                    etOtroContact.setText(contact.getContactRelationshipOther());
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initSpinnerTipoDoc() {
        String[] tipoDocs = new String[]{"DNI", "Carné de Extranjería"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tipoDocs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoDocContact.setAdapter(adapter);
    }

    private void initFlags() {
        flagName = false;
        flagLastName = false;
        flagNumberDocument = false;
        flagTelephone = false;
        flagEmail = false;
        flagTipoParentesco = false;
        flagOtroNombre = false;
    }

    @OnClick({R.id.ll_tutor, R.id.ll_otro, R.id.ll_padre, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_tutor:
                if (rbnPadre.isChecked()) {
                    //     rbnPadre.setChecked(false);
                    //    flagTipoParentesco=false;
                } else {
                    rbnPadre.setChecked(true);
                    rbnTutor.setChecked(false);
                    rbnOtro.setChecked(false);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    flagTipoParentesco=true;
                }
                break;
            case R.id.ll_otro:
                if (rbnOtro.isChecked()) {
                    //    rbnOtro.setChecked(false);
                    //   etOtroContact.setEnabled(false);
                    //  etOtroContact.setText("");
                    // flagTipoParentesco=false;
                } else {
                    rbnOtro.setChecked(true);
                    etOtroContact.setEnabled(true);
                    rbnPadre.setChecked(false);
                    rbnTutor.setChecked(false);
                    flagTipoParentesco=true;
                }

                break;
            case R.id.ll_padre:
                if (rbnPadre.isChecked()) {
                    //         rbnPadre.setChecked(false);
                    //        flagTipoParentesco=false;
                } else {
                    rbnPadre.setChecked(true);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    rbnOtro.setChecked(false);
                    rbnTutor.setChecked(false);
                    flagTipoParentesco=true;
                }

                break;
            case R.id.btn_save:
                validateInputs();
                break;
        }
    }

    private void validateInputs() {
        boolean flag = false;
        boolean flagNameT = false, flagLastNameT = false, flagNumberDocumentT = false, flagAddressT=false, flagTelephoneT = false, flagOtroT=false, flagTipoParentescoT=false;

        if(!flagOtroNombre){
            rbnPadre.requestFocus();
            tvErrorNomContact.setText("Este campo no puede estar vacío");
            flagOtroT=false;
        } else{
            flagOtroT=true;
        }

        if(!flagTipoParentesco){
            rbnPadre.requestFocus();
            tvErrorParentescoContact.setText("Seleccione uno obligatorio");
            flagTipoParentescoT=false;
        } else{
            flagTipoParentescoT=true;
        }


        if (!flagTelephone) {
            etTelephoneContact.requestFocus();
            tvErrorTelephoneContact.setText("Este campo es obligatorio");
            flagTelephoneT = false;
        } else {
            flagTelephoneT = true;
        }

        if (!flagNumberDocument) {
            etNumberDocumentContact.requestFocus();
            tvErrorNumberDocContact.setText("Este campo es obligatorio");
            flagNumberDocumentT = false;
        } else {
            flagNumberDocumentT = true;
        }

        if (!flagLastName) {
            etLastnameContact.requestFocus();
            tvErrorLastnameContact.setText("Este campo es obligatorio");
            flagLastNameT = false;
        } else {
            flagLastNameT = true;
        }

        if (!flagName) {
            etNameContact.requestFocus();
            tvErrorNameContact.setText("Este campo es obligatorio");
            flagNameT = false;
        } else {
            flagNameT = true;
        }

        if (!flagEmail) {
            etEmailContact.requestFocus();
            tvErrorEmailContact.setText("Correo incorrecto");
            flagAddressT = false;
        } else {
            flagAddressT = true;
        }

        if (flagNameT && flagLastNameT && flagAddressT && flagNumberDocumentT && flagTelephoneT  && flagTipoParentescoT && flagOtroT) {
            guardarCambios();
        } else {
            Toast.makeText(getApplicationContext(), "Faltan completar datos", Toast.LENGTH_SHORT).show();
        }

    }

    private boolean validateEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void hideKeyboard() {
        InputMethodManager imm = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
        imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

    }


    //Toolbar
    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarActualizarDatosContacto);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                validateInputs();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    private void guardarCambios() {
        presenter=new UserPresenter(getApplicationContext());
        ContactUpdate contactUpdate = new ContactUpdate();
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        contactUpdate.setIdPerson(registerManager.getIdPerson());
        contactUpdate.setContactFirstName(etNameContact.getText().toString());
        contactUpdate.setContactLastName(etLastnameContact.getText().toString());
        contactUpdate.setContactDocumentType(spTipoDocContact.getSelectedItemPosition()+1);
        contactUpdate.setContactDocumentNumber(etNumberDocumentContact.getText().toString());
        contactUpdate.setContactPhone(etTelephoneContact.getText().toString());
        contactUpdate.setContactEmail(etEmailContact.getText().toString());
        contactUpdate.setContactRelationship(relationship);
        if(relationship==3) contactUpdate.setContactRelationshipOther(etOtroContact.getText().toString());
        else contactUpdate.setContactRelationshipOther("");

        presenter.contactUpdate(contactUpdate);
    }
}
