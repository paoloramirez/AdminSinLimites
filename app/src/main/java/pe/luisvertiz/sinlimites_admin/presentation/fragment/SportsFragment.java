package pe.luisvertiz.sinlimites_admin.presentation.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pe.luisvertiz.sinlimites_admin.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SportsFragment extends Fragment  implements BaseSliderView.OnSliderClickListener{


    @BindView(R.id.slider)
    SliderLayout slider;
    Unbinder unbinder;

    public SportsFragment() {
        // Required empty public constructor
    }

    public static SportsFragment newInstance() {
        SportsFragment fragment = new SportsFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sports, container, false);
        unbinder = ButterKnife.bind(this, view);

     /*   TextSliderView textSliderView = new TextSliderView(getActivity());
        // initialize a SliderLayout
        textSliderView
                .image("https://i.pinimg.com/originals/62/d2/5e/62d25e35b9979e7d05efdec40c2b365f.jpg")
                .setScaleType(BaseSliderView.ScaleType.Fit)
                .setOnSliderClickListener(this);

        slider.addSlider(textSliderView); */

        TextSliderView textSliderView = new TextSliderView(getActivity());
        // initialize a SliderLayout
        textSliderView
                .image(R.drawable.deporte1)
                .setScaleType(BaseSliderView.ScaleType.Fit)
                .setOnSliderClickListener(this);

        slider.addSlider(textSliderView);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
