package pe.luisvertiz.sinlimites_admin.presentation.contract;

import pe.luisvertiz.sinlimites_admin.data.entity.PersonRegister;

/**
 * Created by Luis on 07/02/2018.
 */

public interface UserContract {
    void signUp(String email, String password);

    void login(String email, String password);

    void register(PersonRegister personRegister);

    void getAll();
}
