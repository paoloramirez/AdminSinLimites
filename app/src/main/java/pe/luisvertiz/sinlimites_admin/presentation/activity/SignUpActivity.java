package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_signup)
    Toolbar toolbarSignup;
    @BindView(R.id.btn_signup)
    AppCompatButton btnSignUp;
    @BindView(R.id.et_correo_register)
    TextInputEditText etCorreoRegister;
    @BindView(R.id.et_password_register)
    TextInputEditText etPasswordRegister;
    @BindView(R.id.et_confirm_password_register)
    TextInputEditText etConfirmPasswordRegister;
    @BindView(R.id.til_correo_register)
    TextInputLayout tilCorreoRegister;
    @BindView(R.id.til_password_register)
    TextInputLayout tilPasswordRegister;
    @BindView(R.id.til_confirm_password_register)
    TextInputLayout tilConfirmPasswordRegister;

    boolean correoRegister=false;
    private UserPresenter userPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        showToolbar("Crear Cuenta", true);

   //     initErrors();

        etCorreoRegister.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!validateEmail(s.toString())) {
                    tilCorreoRegister.setError("Correo incorrecto");
                } else{
                    tilCorreoRegister.setError("");
                }

            }
        });

        etPasswordRegister.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()<5){
                    tilPasswordRegister.setError("Contraseña mínimo 5 caracteres");
                } else{
                    if(s.toString().equals(etConfirmPasswordRegister.getText().toString())){
                        tilConfirmPasswordRegister.setError("");
                    } else{
                        tilConfirmPasswordRegister.setError("Las contraseñas no coinciden");
                    }
                    tilPasswordRegister.setError("");
                }
            }
        });

        etConfirmPasswordRegister.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()<5){
                    tilConfirmPasswordRegister.setError("Contraseña mínimo 5 caracteres");
                } else{
                    if(!s.toString().equals(etPasswordRegister.getText().toString())){
                        tilConfirmPasswordRegister.setError("Las contraseñas no coinciden");
                    } else{
                        tilConfirmPasswordRegister.setError("");
                    }
                }



            }
        });

    }

    private void initErrors() {
        tilCorreoRegister.setError("Correo incorrecto");
        tilPasswordRegister.setError("Contraseña minimo 5 caracteres");
    }

    private boolean validateEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @OnClick(R.id.btn_signup)
    public void onViewClicked() {

        boolean flag=false;

        if(etConfirmPasswordRegister.length()>=5){
            flag=true;
            if(etConfirmPasswordRegister.getText().toString().equals(etPasswordRegister.getText().toString())){
                flag=true;
            } else{
                flag=false;
                tilConfirmPasswordRegister.setError("Las contraseñas no coinciden");
                etConfirmPasswordRegister.requestFocus();
            }
        }

        if(etPasswordRegister.length()>=5){
            flag=true;
        } else{
            flag=false;
            tilPasswordRegister.setError("Contraseña mínimo 5 caracteres");
            etPasswordRegister.requestFocus();
        }

        if(validateEmail(etCorreoRegister.getText().toString())){
            flag=true;
        } else{
            flag=false;
            tilCorreoRegister.setError("Correo incorrecto");
            etCorreoRegister.requestFocus();
        }





        if(flag){
            userPresenter=new UserPresenter(getApplicationContext());
            userPresenter.signUp(etCorreoRegister.getText().toString(), etPasswordRegister.getText().toString());
        } else{
            Toast.makeText(getApplicationContext(), "No se puede registrar", Toast.LENGTH_SHORT).show();
        }

    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);

    }

    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarSignup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
