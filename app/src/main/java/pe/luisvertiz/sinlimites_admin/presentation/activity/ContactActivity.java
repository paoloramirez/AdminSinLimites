package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonRegister;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

public class ContactActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_contact)
    Toolbar toolbarContact;
    @BindView(R.id.et_name_contact)
    EditText etNameContact;
    @BindView(R.id.et_lastname_contact)
    EditText etLastnameContact;
    @BindView(R.id.et_number_document_contact)
    EditText etNumberDocumentContact;
    @BindView(R.id.et_telephone_contact)
    EditText etTelephoneContact;
    @BindView(R.id.et_email_contact)
    EditText etEmailContact;
    @BindView(R.id.rbn_padre)
    AppCompatRadioButton rbnPadre;
    @BindView(R.id.rbn_tutor)
    AppCompatRadioButton rbnTutor;
    @BindView(R.id.ll_tutor)
    LinearLayout llTutor;
    @BindView(R.id.rbn_otro)
    AppCompatRadioButton rbnOtro;
    @BindView(R.id.ll_otro)
    LinearLayout llOtro;
    @BindView(R.id.ll_padre)
    LinearLayout llPadre;
    @BindView(R.id.et_otro_contact)
    EditText etOtroContact;
    @BindView(R.id.btn_save)
    AppCompatButton btnSave;

    boolean flagName, flagLastName, flagNumberDocument, flagTelephone, flagAddress, flagTipoParentesco, flagOtroNombre;
    @BindView(R.id.sp_tipo_doc_contact)
    AppCompatSpinner spTipoDocContact;
    @BindView(R.id.tv_error_name_contact)
    TextView tvErrorNameContact;
    @BindView(R.id.tv_error_lastname_contact)
    TextView tvErrorLastnameContact;
    @BindView(R.id.tv_error_number_doc_contact)
    TextView tvErrorNumberDocContact;
    @BindView(R.id.tv_error_telephone_contact)
    TextView tvErrorTelephoneContact;
    @BindView(R.id.tv_error_email_contact)
    TextView tvErrorEmailContact;
    @BindView(R.id.tv_error_parentesco_contact)
    TextView tvErrorParentescoContact;
    @BindView(R.id.tv_error_nom_contact)
    TextView tvErrorNomContact;

    private UserPresenter presenter;

    int relationship = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);
        etOtroContact.setEnabled(false);

        showToolbar("Datos de Contacto", true);

        initFlags();

        etNameContact.requestFocus();
        initSpinnerTipoDoc();

        etNameContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etNameContact.length() == 0) {
                        tvErrorNameContact.setText("Este campo es obligatorio");
                    } else {
                        etLastnameContact.requestFocus();
                    }
                }
                return true;
            }
        });

        etNameContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    tvErrorNameContact.setText("Este campo es obligatorio");
                    flagName = false;
                } else {
                    tvErrorNameContact.setText("");
                    flagName = true;
                }

            }
        });

        etLastnameContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etLastnameContact.length() == 0) {
                        tvErrorLastnameContact.setText("Este campo es obligatorio");
                    } else {
                        etNumberDocumentContact.requestFocus();
                    }
                }
                return true;
            }
        });

        etLastnameContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    tvErrorLastnameContact.setText("Este campo es obligatorio");
                    flagLastName = false;
                } else {
                    tvErrorLastnameContact.setText("");
                    flagLastName = true;
                }

            }
        });

        etNumberDocumentContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etNumberDocumentContact.length() == 0) {
                        tvErrorNumberDocContact.setText("Este campo es obligatorio");
                    }
                    etTelephoneContact.requestFocus();

                }
                return true;
            }
        });

        etNumberDocumentContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagNumberDocument = false;
                    tvErrorNumberDocContact.setText("Este campo es obligatorio");
                } else {
                    if (spTipoDocContact.getSelectedItemPosition() == 0) {
                        if (s.length() != 8) {
                            flagNumberDocument = false;
                            tvErrorNumberDocContact.setText("Es necesario 8 digitos");
                        } else {
                            flagNumberDocument = true;
                            tvErrorNumberDocContact.setText("");
                        }


                    } else if (spTipoDocContact.getSelectedItemPosition() == 1) {
                        if (s.length() != 9) {
                            flagNumberDocument = false;
                            tvErrorNumberDocContact.setText("Es necesario 9 digitos");
                        } else {
                            flagNumberDocument = true;
                            tvErrorNumberDocContact.setText("");
                        }

                    }

                }
            }

        });

        etTelephoneContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (etTelephoneContact.length() == 0) {
                        tvErrorTelephoneContact.setText("Este campo es obligatorio");
                    } else {
                        etEmailContact.requestFocus();
                    }


                }
                return true;
            }
        });

        etTelephoneContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    flagTelephone = false;
                    tvErrorTelephoneContact.setText("Este campo es obligatorio");
                } else if (s.length() != 9) {
                    flagTelephone = false;
                    tvErrorTelephoneContact.setText("Es necesario 9 digitos");
                } else {
                    flagTelephone = true;
                    tvErrorTelephoneContact.setText("");
                }
            }
        });

        etEmailContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                 /*   if (etTelephoneContact.length() == 0) {
                        tvErrorTelephoneContact.setText("Este campo es obligatorio");
                    } else { */
                    rbnPadre.requestFocus();
                    hideKeyboard();
                    //   }
                }
                return true;
            }
        });

        etEmailContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0) {
                    if (!validateEmail(s.toString())) {
                        tvErrorEmailContact.setText("Correo incorrecto");
                        flagAddress = false;
                    } else {
                        tvErrorEmailContact.setText("");
                        flagAddress = true;
                    }
                }

            }
        });


        rbnPadre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relationship = 1;
                    rbnTutor.setChecked(false);
                    rbnOtro.setChecked(false);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    tvErrorParentescoContact.setText("");
                    //       flagOtroNombre=false;
                    flagTipoParentesco=true;
                    flagOtroNombre=true;
                } else{
                    //     flagOtroNombre=false;
                    //    tvErrorParentescoContact.setText("Seleccione uno obligatorio");
                    //   flagTipoParentesco=false;
                }
                tvErrorNomContact.setText("");
                hideKeyboard();
            }
        });

        rbnTutor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relationship = 2;
                    rbnPadre.setChecked(false);
                    rbnOtro.setChecked(false);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    tvErrorParentescoContact.setText("");
                    //     flagOtroNombre=false;
                    flagTipoParentesco=true;
                    flagOtroNombre=true;
                } else{
                    //       flagTipoParentesco=false;
                    //      tvErrorParentescoContact.setText("Seleccione uno obligatorio");
                    //    flagOtroNombre=false;
                }
                tvErrorNomContact.setText("");
                hideKeyboard();
            }
        });

        rbnOtro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relationship = 3;
                    rbnPadre.setChecked(false);
                    rbnTutor.setChecked(false);
                    etOtroContact.setEnabled(true);
                    etOtroContact.setText("");
                    etOtroContact.requestFocus();
                    //    flagOtroNombre=true;
                    tvErrorParentescoContact.setText("");
                    flagTipoParentesco=true;
                    showKeyboard();
                    tvErrorNomContact.setText("Nombre incorrecto");

                } else {
                    //        etOtroContact.setEnabled(false);
                    //       etOtroContact.setText("");
                    //  flagOtroNombre=false;
                    //         tvErrorParentescoContact.setText("Seleccione uno obligatorio");
                    //        flagTipoParentesco=true;
                    tvErrorNomContact.setText("");
                    hideKeyboard();
                }
            }
        });

        etOtroContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (etOtroContact.length() == 0) {
                        tvErrorNomContact.setText("Este campo no puede estar vacío");
                    } else {
                        tvErrorNomContact.setText("");
                    }

                }
                return true;
            }
        });

        etOtroContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(etOtroContact.isEnabled())
                {
                    if(s.length()==0){
                        flagOtroNombre=false;
                        tvErrorNomContact.setText("Nombre incorrecto");
                    } else{
                        flagOtroNombre=true;
                        tvErrorNomContact.setText("");
                    }
                }

            }
        });

    }

    private void initSpinnerTipoDoc() {
        String[] tipoDocs = new String[]{"DNI", "Carné de Extranjería"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, tipoDocs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTipoDocContact.setAdapter(adapter);
    }

    private void initFlags() {
        flagName = false;
        flagLastName = false;
        flagNumberDocument = false;
        flagTelephone = false;
        flagAddress = false;
        flagTipoParentesco = false;
        flagOtroNombre = false;


    }

    @OnClick({R.id.ll_tutor, R.id.ll_otro, R.id.ll_padre, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_tutor:
                if (rbnPadre.isChecked()) {
                    //     rbnPadre.setChecked(false);
                    //    flagTipoParentesco=false;
                } else {
                    rbnPadre.setChecked(true);
                    rbnTutor.setChecked(false);
                    rbnOtro.setChecked(false);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    flagTipoParentesco=true;
                }
                break;
            case R.id.ll_otro:
                if (rbnOtro.isChecked()) {
                    //    rbnOtro.setChecked(false);
                    //   etOtroContact.setEnabled(false);
                    //  etOtroContact.setText("");
                    // flagTipoParentesco=false;
                } else {
                    rbnOtro.setChecked(true);
                    etOtroContact.setEnabled(true);
                    rbnPadre.setChecked(false);
                    rbnTutor.setChecked(false);
                    flagTipoParentesco=true;
                }

                break;
            case R.id.ll_padre:
                if (rbnPadre.isChecked()) {
                    //         rbnPadre.setChecked(false);
                    //        flagTipoParentesco=false;
                } else {
                    rbnPadre.setChecked(true);
                    etOtroContact.setEnabled(false);
                    etOtroContact.setText("");
                    rbnOtro.setChecked(false);
                    rbnTutor.setChecked(false);
                    flagTipoParentesco=true;
                }

                break;
            case R.id.btn_save:

                validateInputs();

                break;
        }
    }

    private void validateInputs() {
        boolean flag = false;
        boolean flagNameT = false, flagLastNameT = false, flagNumberDocumentT = false, flagAddressT=false, flagTelephoneT = false, flagOtroT=false, flagTipoParentescoT=false;

        if(!flagOtroNombre){
            rbnPadre.requestFocus();
            tvErrorNomContact.setText("Este campo no puede estar vacío");
            flagOtroT=false;
        } else{
            flagOtroT=true;
        }

        if(!flagTipoParentesco){
            rbnPadre.requestFocus();
            tvErrorParentescoContact.setText("Seleccione uno obligatorio");
            flagTipoParentescoT=false;
        } else{
            flagTipoParentescoT=true;
        }


        if (!flagTelephone) {
            etTelephoneContact.requestFocus();
            tvErrorTelephoneContact.setText("Este campo es obligatorio");
            flagTelephoneT = false;
        } else {
            flagTelephoneT = true;
        }

        if (!flagNumberDocument) {
            etNumberDocumentContact.requestFocus();
            tvErrorNumberDocContact.setText("Este campo es obligatorio");
            flagNumberDocumentT = false;
        } else {
            flagNumberDocumentT = true;
        }

        if (!flagLastName) {
            etLastnameContact.requestFocus();
            tvErrorLastnameContact.setText("Este campo es obligatorio");
            flagLastNameT = false;
        } else {
            flagLastNameT = true;
        }



        if (!flagName) {
            etNameContact.requestFocus();
            tvErrorNameContact.setText("Este campo es obligatorio");
            flagNameT = false;
        } else {
            flagNameT = true;
        }

        if (!flagAddress) {
            etEmailContact.requestFocus();
            tvErrorEmailContact.setText("Correo incorrecto");
            flagAddressT = false;
        } else {
            flagAddressT = true;
        }

        if (flagNameT && flagLastNameT && flagAddressT && flagNumberDocumentT && flagTelephoneT  && flagTipoParentescoT && flagOtroT) {
            saveData();
        } else {
            Toast.makeText(getApplicationContext(), "Faltan completar datos", Toast.LENGTH_SHORT).show();
        }

    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);
    }

    private void saveData() {
        /**Session Manager**/
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        registerManager.setContactName(etNameContact.getText().toString());
        registerManager.setContactLastName(etLastnameContact.getText().toString());
        registerManager.setContactCodeTipoCod(spTipoDocContact.getSelectedItemPosition()+1);
        registerManager.setContactNumberDocument(etNumberDocumentContact.getText().toString());
        registerManager.setContactTelephone(etTelephoneContact.getText().toString());
        registerManager.setContactEmail(etEmailContact.getText().toString());
        registerManager.setTypeRelationship(relationship);

        if (relationship == 3)
        {
            registerManager.setContactOtherRelationship(etOtroContact.getText().toString());
        }

        Test();

        sendUser();
    }

    private void sendUser() {
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        presenter=new UserPresenter(getApplicationContext());

        PersonRegister personRegister=new PersonRegister();

        personRegister.setIdUser(registerManager.getIdUser());

        //Register1
        personRegister.setFirstName(registerManager.getName());
        personRegister.setLastName(registerManager.getLastName());
        personRegister.setBirthdate(registerManager.getBirthday());
        personRegister.setGender(registerManager.getGenero());
        personRegister.setDocumentType(registerManager.getCodeTipoCod());
        personRegister.setDocumentNumber(Integer.parseInt(registerManager.getNumberDocument()));
        personRegister.setPhone(Integer.parseInt(registerManager.getTelephone()));
        personRegister.setDistrict(registerManager.getCodeDistrict());
        personRegister.setAddress(registerManager.getAddress());

        //Register2
        personRegister.setCodeConadis(registerManager.getCodeConadis());
        personRegister.setPersonDisabilityType(registerManager.getDisability());
        personRegister.setDisabilityReason(registerManager.getHistory());
        personRegister.setPersonHealthInsurableType(registerManager.getTypeSafe());

        //Interest
        personRegister.setPersonSocialNetwork(registerManager.getSocialNetwork());
        personRegister.setPersonTechnologicalDevice(registerManager.getDevices());

        personRegister.setPersonFavoriteTechnologicalDevice(Integer.toString(registerManager.getPreferenceDevice()));
        personRegister.setPersonEventSport(registerManager.getSports());
        personRegister.setPersonInterestedSport(registerManager.getPreferencesSports());

        //Contact
        personRegister.setContactFirstName(registerManager.getContactName());
        personRegister.setContactLastName(registerManager.getContactLastName());
        personRegister.setContactDocumentType(registerManager.getContactCodeTipoCod());
        personRegister.setContactDocumentNumber(registerManager.getContactNumberDocument());
        personRegister.setContactPhone(registerManager.getContactTelephone());
        personRegister.setContactEmail(registerManager.getContactEmail());
        personRegister.setContactRelationship(registerManager.getTypeRelationship());

        int test_rs = registerManager.getTypeRelationship();
        String test_relationship = "";
        switch (test_rs)
        {
            case 3:
                test_relationship = registerManager.getContactOtherRelationship();
                break;
            default:
                test_relationship = "";
                break;
        }
        personRegister.setContactRelationshipOther(test_relationship);
        presenter.register(personRegister);
    }

    private void Test() {
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());


        Log.d("REGISTER 1", "----------------DATOS-----------\n" +
                registerManager.getName()+"\n"+
                registerManager.getLastName()+"\n"+
                registerManager.getBirthday()+"\n"+
                registerManager.getGenero()+"\n"+
                registerManager.getCodeTipoCod()+"\n"+
                registerManager.getNumberDocument()+"\n"+
                registerManager.getTelephone()+"\n"+
                registerManager.getCodeDistrict()+"\n"+
                registerManager.getAddress()+"\n");


        Log.d("REGISTER 2", "----------------DATOS---------\n" +
                registerManager.getCodeConadis()+"\n"+
                registerManager.getDisability()+"\n"+
                registerManager.getHistory()+"\n"+
                registerManager.getTypeSafe()+"\n");

        Log.d("INTERESTS", "----------------DATOS---------\n" +
                registerManager.getSocialNetwork() + "\n" +
                registerManager.getDevices() + "\n" +
                registerManager.getPreferenceDevice() + "\n" +
                registerManager.getSports() + "\n" +
                registerManager.getPreferencesSports() + "\n");

        int test_rs = registerManager.getTypeRelationship();
        String test_relationship = "";
        switch (test_rs)
        {
            case 3:
                test_relationship = registerManager.getContactOtherRelationship();
                break;
            default:
                test_relationship = Integer.toString(test_rs);
                break;
        }

        Log.d("CONTACT", "------------------DATOS----------\n" +
                registerManager.getContactName() + "\n" +
                registerManager.getContactLastName() + "\n" +
                registerManager.getContactCodeTipoCod() + "\n" +
                registerManager.getContactNumberDocument() + "\n" +
                registerManager.getContactTelephone() + "\n" +
                registerManager.getContactEmail() + "\n" +
                test_relationship + "\n");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarContact);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    private boolean validateEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void hideKeyboard() {
        InputMethodManager imm = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));
        imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
    }

    private void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

    }
}
