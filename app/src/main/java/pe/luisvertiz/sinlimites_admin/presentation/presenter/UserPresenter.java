package pe.luisvertiz.sinlimites_admin.presentation.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import pe.luisvertiz.sinlimites_admin.data.entity.Athlete;
import pe.luisvertiz.sinlimites_admin.data.entity.AthleteList;
import pe.luisvertiz.sinlimites_admin.data.entity.AthleteMin;
import pe.luisvertiz.sinlimites_admin.data.entity.Contact;
import pe.luisvertiz.sinlimites_admin.data.entity.ContactUpdate;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonDetail;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonHealthUpdate;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonRegister;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonUpdate;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.data.repository.remote.ServiceGenerator;
import pe.luisvertiz.sinlimites_admin.data.repository.remote.request.UserRequest;
import pe.luisvertiz.sinlimites_admin.presentation.activity.ContactActivity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.EditContactDataActivity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.ListAthletesActivity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.Main2Activity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.MainActivity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.PerfilActivity;
import pe.luisvertiz.sinlimites_admin.presentation.activity.PersonalDataDetailActivity;
import pe.luisvertiz.sinlimites_admin.presentation.contract.UserContract;
import pe.luisvertiz.sinlimites_admin.presentation.fragment.ListAthletes1Fragment;
import pe.luisvertiz.sinlimites_admin.presentation.fragment.ListAthletes2Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Luis on 07/02/2018.
 */

public class UserPresenter implements UserContract {
    private Context context;
    private AthleteList athleteList;

    private ArrayList<AthleteMin> lista1;
    private ArrayList<AthleteMin> lista2;
    private ListAthletesActivity.SectionsPagerAdapter mSectionsPagerAdapter;

    //test
    private int test;
    private ListAthletes1Fragment athlete1;
    private ListAthletes2Fragment athlete2;


    /*
     *  Detalle Deportista
     */
    //PersonDetail get
    private PersonDetail person;

    //TextView a editar
    private TextView tvNombres;
    private TextView tvDocIdentidad, tvFechNacimiento, tvDireccion;
    private TextView tvMisDeportes;
    private TextView tvNomContacto, tvApeContacto, tvDocIdentidadContacto, tvCelContacto, tvCorreoContacto, tvTipoParentesco;

    //TextView DatosPersonales Detalle
    private TextView tvNomDetail, tvFechNacimientoDetail, tvGenero,
            tvDocIdentidadDetail, tvTelefonoDetail, tvUbicacion,
            tvDireccionDetail;
    private TextView tvCodConadis, tvTipoDiscapacidades, tvTipoSeguro;

    private int tipo_request;


    /*
     * Constructores
     */

    public UserPresenter(Context context) {
        this.context = context;
        this.lista1 = new ArrayList<>();
        this.lista2 = new ArrayList<>();
        this.test = 0;
    }

    public UserPresenter(Context context, ListAthletesActivity.SectionsPagerAdapter mSectionsPagerAdapter, ListAthletes1Fragment athlete1, ListAthletes2Fragment athlete2) {
        this.context = context;
        this.lista1 = new ArrayList<>();
        this.lista2 = new ArrayList<>();
        this.test = 0;
        this.mSectionsPagerAdapter = mSectionsPagerAdapter;
        this.athlete1 = athlete1;
        this.athlete2 = athlete2;
    }

    public UserPresenter(Context context, PersonDetail person, TextView tvNombres,
                         TextView tvDocIdentidad, TextView tvFechNacimiento, TextView tvDireccion,
                         TextView tvMisDeportes,
                         TextView tvNomContacto, TextView tvApeContacto, TextView tvDocIdentidadContacto, TextView tvCelContacto, TextView tvCorreoContacto, TextView tvTipoParentesco) {
        this.context = context;
        this.person = person;
        this.tipo_request = 1;
        /*
         * Detail
         */
        this.tvNombres = tvNombres;

        //Datos Personales
        this.tvDocIdentidad = tvDocIdentidad;
        this.tvFechNacimiento = tvFechNacimiento;
        this.tvDireccion = tvDireccion;

        //Deportes
        this.tvMisDeportes = tvMisDeportes;

        //Contacto
        this.tvNomContacto = tvNomContacto;
        this.tvApeContacto = tvApeContacto;
        this.tvDocIdentidadContacto = tvDocIdentidadContacto;
        this.tvCelContacto = tvCelContacto;
        this.tvCorreoContacto = tvCorreoContacto;
        this.tvTipoParentesco = tvTipoParentesco;

    }

    public UserPresenter(PersonDetail person, Context context, TextView tvNomDetail,
                         TextView tvFechNacimientoDetail, TextView tvGenero, TextView tvDocIdentidadDetail, TextView tvTelefonoDetail, TextView tvUbicacion, TextView tvDireccionDetail,
                         TextView tvCodConadis, TextView tvTipoDiscapacidades, TextView tvTipoSeguro) {
        this.context = context;
        this.person = person;
        this.tipo_request = 2;
        /*
         * Detail
         */
        //Datos
        this.tvNomDetail = tvNomDetail;
        this.tvFechNacimientoDetail = tvFechNacimientoDetail;
        this.tvGenero = tvGenero;
        this.tvDocIdentidadDetail = tvDocIdentidadDetail;
        this.tvTelefonoDetail = tvTelefonoDetail;
        this.tvUbicacion = tvUbicacion;
        this.tvDireccionDetail = tvDireccionDetail;

        //Informacion de discapacidad
        this.tvCodConadis = tvCodConadis;
        this.tvTipoDiscapacidades = tvTipoDiscapacidades;
        this.tvTipoSeguro = tvTipoSeguro;
    }


    @Override
    public void signUp(String email, String password) {
        UserRequest userRequest= ServiceGenerator.createService(UserRequest.class);
        Call<JsonObject> call=userRequest.signUp(email, password);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    JsonObject jsonObject=response.body();


                    int status=jsonObject.get("status").getAsInt();

                    switch (status){
                        case 2:   Toast.makeText(context, "Usuario ya Registrado", Toast.LENGTH_SHORT).show(); break;
                        case 1: Toast.makeText(context, "Registrado correctamente", Toast.LENGTH_SHORT).show();  RegisterManager registerManager = RegisterManager.getInstance(context);
                            registerManager.setIdUser(jsonObject.get("idUser").getAsInt()); registerManager.login(); launchActivity(context, MainActivity.class); break;
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void login(String email, String password) {
        UserRequest userRequest=ServiceGenerator.createService(UserRequest.class);
        Call<JsonObject> call=userRequest.login(email, password);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    JsonObject jsonObject=response.body();
                    int status=jsonObject.get("status").getAsInt();

                    switch (status){
                        case 1:   Toast.makeText(context, "Acceso correcto", Toast.LENGTH_SHORT).show(); RegisterManager registerManager = RegisterManager.getInstance(context); registerManager.setIdUser(jsonObject.get("idUser").getAsInt()); registerManager.login(); launchActivity(context, Main2Activity.class); break;
                        case 2: Toast.makeText(context, "Usuario y/o Contraseña incorrecto", Toast.LENGTH_SHORT).show(); break;

                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void register(PersonRegister personRegister) {
        UserRequest userRequest = ServiceGenerator.createService(UserRequest.class);
        Call<JsonObject> call = userRequest.register(personRegister);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        try {
                            JsonObject jsonObject = response.body();
                            JsonObject jsonAthlete = (JsonObject) jsonObject.get("athlete");
                            int status = jsonObject.get("status").getAsInt();
                            Log.e("STATUS", status + "");

                            switch ( status)
                            {
                                case 1:
                                    Toast.makeText(context, "REGISTADO CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                                    RegisterManager registerManager = RegisterManager.getInstance(context);
                                    registerManager.setIdPerson(jsonAthlete.get("idPerson").getAsInt());
                                    Log.e("ID PERSON",Integer.toString(jsonAthlete.get("idPerson").getAsInt()));
                                    launchActivity(context, MainActivity.class);
                                    break;

                                default:
                                    String error = jsonObject.get("error").getAsString();
                                    Log.e("MESSAGE", error);
                                    Toast.makeText(context, status + "" + error, Toast.LENGTH_SHORT).show();
                                    break;
                            }

                        } catch (Exception e) {
                            Toast.makeText(context, "HA OCURRIDO UN ERROR AL REGISTRARSE", Toast.LENGTH_SHORT).show();
                            launchActivity(context, ContactActivity.class);
                        }


                    } else {
                        Toast.makeText(context, "Nulo", Toast.LENGTH_SHORT).show();
                    }

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void getAll() {
        UserRequest userRequest=ServiceGenerator.createService(UserRequest.class);
        Call<AthleteList> call=userRequest.getAll();
        call.enqueue(new Callback<AthleteList>() {
            @Override
            public void onResponse(Call<AthleteList> call, Response<AthleteList> response) {
                if(response.isSuccessful()){
                    athleteList =response.body();

                    ArrayList<AthleteMin> lista1 = new ArrayList<>();
                    ArrayList<AthleteMin> lista2 = new ArrayList<>();

                    if (athleteList.getAthletes() != null)
                    {
                        for (AthleteMin athleteMin : athleteList.getAthletes())
                        {
                            if (athleteMin.getPersonQualifiedStatus() != 0)
                            {
                                lista1.add(athleteMin);
                            }
                            else {
                                lista2.add(athleteMin);
                            }
                        }
                        Toast.makeText(context, "CARGO LISTA", Toast.LENGTH_SHORT).show();
                        setLista1(lista1);
                        setLista2(lista2);
                        CargarAthletes1Test(lista1);
                        CargarAthletes2Test(lista2);
                        mSectionsPagerAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(context, "No se han obtenido deportistas", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<AthleteList> call, Throwable t) {

                Toast.makeText(context, "No se puede conectar.", Toast.LENGTH_SHORT).show();

            }
        });

    }


    public void contactUpdate(ContactUpdate contactUpdate) {
        UserRequest userRequest = ServiceGenerator.createService(UserRequest.class);
        Call<JsonObject> call = userRequest.contactUpdate(contactUpdate);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        try {
                            JsonObject jsonObject = response.body();
                            int status = jsonObject.get("status").getAsInt();
                            Log.e("STATUS", status + "");
                            if (status != 1) {
                                String error = jsonObject.get("error").getAsString();
                                Log.e("MESSAGE", error);
                                Toast.makeText(context, status + "" + error, Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(context, "DATOS ACTUALIZADOS CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                            launchActivity(context, PerfilActivity.class);
                        } catch (Exception e) {
                            Toast.makeText(context, "HA OCURRIDO UN ERROR AL ACTUALIZAR SUS DATOS", Toast.LENGTH_SHORT).show();
                            launchActivity(context, EditContactDataActivity.class);
                        }

                    } else {
                        Toast.makeText(context, "Nulo", Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void personUpdate(PersonUpdate personUpdate) {
        UserRequest userRequest = ServiceGenerator.createService(UserRequest.class);
        Call<JsonObject> call = userRequest.personUpdate(personUpdate);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        try {
                            JsonObject jsonObject = response.body();
                            int status = jsonObject.get("status").getAsInt();
                            Log.e("STATUS", status + "");
                            if (status != 1) {
                                String error = jsonObject.get("error").getAsString();
                                Log.e("MESSAGE", error);
                                Toast.makeText(context, status + "" + error, Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(context, "DATOS ACTUALIZADOS CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                            launchActivity(context, PersonalDataDetailActivity.class);
                        } catch (Exception e) {
                            Toast.makeText(context, "HA OCURRIDO UN ERROR AL ACTUALIZAR SUS DATOS", Toast.LENGTH_SHORT).show();
                            launchActivity(context, PersonalDataDetailActivity.class);
                        }

                    } else {
                        Toast.makeText(context, "Nulo", Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void personHealthUpdate(PersonHealthUpdate personHealthUpdate) {
        UserRequest userRequest = ServiceGenerator.createService(UserRequest.class);
        Call<JsonObject> call = userRequest.personHealthUpdate(personHealthUpdate);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        try {
                            JsonObject jsonObject = response.body();
                            int status = jsonObject.get("status").getAsInt();
                            Log.e("STATUS", status + "");
                            if (status != 1) {
                                String error = jsonObject.get("error").getAsString();
                                Log.e("MESSAGE", error);
                                Toast.makeText(context, status + "" + error, Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(context, "DATOS ACTUALIZADOS CORRECTAMENTE", Toast.LENGTH_SHORT).show();
                            launchActivity(context, PersonalDataDetailActivity.class);
                        } catch (Exception e) {
                            Toast.makeText(context, "HA OCURRIDO UN ERROR AL ACTUALIZAR SUS DATOS", Toast.LENGTH_SHORT).show();
                            launchActivity(context, PersonalDataDetailActivity.class);
                        }

                    } else {
                        Toast.makeText(context, "Nulo", Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void getDetail(int id) {
        UserRequest userRequest = ServiceGenerator.createService(UserRequest.class);
        Call<PersonDetail> call = userRequest.getDetail(id);

        call.enqueue(new Callback<PersonDetail>() {
            @Override
            public void onResponse(Call<PersonDetail> call, Response<PersonDetail> response) {
                if (response.isSuccessful()) {
                    try {
                        PersonDetail tmp_person = response.body();
                        person = tmp_person;
                        switch (tipo_request){
                            case 1:
                                actualizarDatos();
                                break;
                            case 2:
                                actualizarLabels();
                                break;
                        }
                    } catch (Exception e) {
                        Toast.makeText(context, "Ha ocurrido un error al obtener los datos del Deportista.", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PersonDetail> call, Throwable t) {
                Toast.makeText(context, "No se puede conectar", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /*
     * Otros
     */
    public void filter(String query){
        query = query.toLowerCase();

        final ArrayList<AthleteMin> dataFiltrada = new ArrayList<>();

        if (athleteList.getAthletes() != null)
        {

            for (AthleteMin athleteMin : athleteList.getAthletes()) {

                final String text = (athleteMin.getPersonFirstName()+" "+ athleteMin.getPersonLastName()).toLowerCase();

                if (text.contains(query)) {
                    dataFiltrada.add(athleteMin);
                }
            }
        }

        if (dataFiltrada.size() > 0) {
            //adapter.filter(dataFiltrada, query);
        } else {
            Toast.makeText(context, "No hay resultados", Toast.LENGTH_SHORT).show();
        }
    }

    public PersonDetail getPerson() {
        return person;
    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    //Extras
    //Activity Profile
    private void actualizarDatos() {
        Athlete athlete = person.getAthlete();
        Contact contact = person.getContact();
        /*
         * Detail
         */
        tvNombres.setText((athlete.getPersonFirstName() + ", " + athlete.getPersonLastName()).toUpperCase());
        //Datos Personales
        tvDocIdentidad.setText(athlete.getPersonDocumentNumber());
        tvFechNacimiento.setText(athlete.getPersonBirthDate());
        tvDireccion.setText(athlete.getPersonAddress());

        //Deportes
        if(athlete.getPersonQualifiedStatus() == 1)
            tvMisDeportes.setText(athlete.getSportstoString(person.getQualifiedSports()));
        else
            tvMisDeportes.setText("Aún no ha sido calificado");

        //Contacto
        if (contact!=null)
        {
            tvNomContacto.setText(contact.getContactFirstName());
            tvApeContacto.setText(contact.getContactLastName());
            tvDocIdentidadContacto.setText(contact.getContactDocumentNumber());
            tvCelContacto.setText(contact.getContactPhone());
            tvCorreoContacto.setText(contact.getContactEmail());
            tvTipoParentesco.setText(athlete.getRelationshiptoDetail(contact.getIdRelationship(), contact.getContactRelationshipOther()));
        }

    }

    //Activity PersonDetail
    private void actualizarLabels() {
        try {
            Athlete athlete = person.getAthlete();
            //profileImage;

            //Datos 1
            tvNomDetail.setText(athlete.getPersonFirstName() + ", "+ athlete.getPersonLastName());
            tvFechNacimientoDetail.setText(athlete.getPersonBirthDate());
            tvGenero.setText(athlete.getGenderDetail());
            tvDocIdentidadDetail.setText(athlete.getDocIdentidadDetail());
            tvTelefonoDetail.setText(athlete.getPersonPhone());
            tvUbicacion.setText(athlete.getUbicacionDetail());
            tvDireccionDetail.setText(athlete.getPersonAddress());

            //Datos 2
            tvCodConadis.setText(athlete.getPersonConadisCode());
            tvTipoDiscapacidades.setText(athlete.getDiscapacidadesDetail());
            tvTipoSeguro.setText(athlete.getSegurosDetail());

        } catch (Exception e) {

        }
    }

    public ArrayList<AthleteMin> getLista1() {
        return lista1;
    }

    public void setLista1(ArrayList<AthleteMin> lista1) {
        this.lista1 = lista1;
    }

    public ArrayList<AthleteMin> getLista2() {
        return lista2;
    }

    public void setLista2(ArrayList<AthleteMin> lista2) {
        this.lista2 = lista2;
    }

    //Test
    private void CargarAthletes2Test(ArrayList<AthleteMin> lista2)
    {

        Bundle bundle2=new Bundle();
        bundle2.putSerializable("athletes_2", lista2);
        athlete2.setArguments(bundle2);

    }

    private void CargarAthletes1Test(ArrayList<AthleteMin> lista1) {

        Bundle bundle1=new Bundle();
        bundle1.putSerializable("athletes_1", lista1);
        athlete1.setArguments(bundle1);
    }


}
