package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import pe.luisvertiz.sinlimites_admin.BuildConfig;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonDetail;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class PersonalDataDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_datos_personales)
    Toolbar toolbarDatosPersonales;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.btnEditDetail)
    FloatingActionButton btnEditDetail;
    @BindView(R.id.iconEditDetail1)
    ImageView iconEditDetail1;
    @BindView(R.id.tv_NomDetail)
    TextView tvNomDetail;
    @BindView(R.id.tv_FechNacimientoDetail)
    TextView tvFechNacimientoDetail;
    @BindView(R.id.tv_Genero)
    TextView tvGenero;
    @BindView(R.id.tv_DocIdentidadDetail)
    TextView tvDocIdentidadDetail;
    @BindView(R.id.tv_TelefonoDetail)
    TextView tvTelefonoDetail;
    @BindView(R.id.tv_Ubicacion)
    TextView tvUbicacion;
    @BindView(R.id.tv_Direccion)
    TextView tvDireccion;
    @BindView(R.id.iconEditDetail2)
    ImageView iconEditDetail2;
    @BindView(R.id.tv_CodConadis)
    TextView tvCodConadis;
    @BindView(R.id.tv_TipoDiscapacidades)
    TextView tvTipoDiscapacidades;
    @BindView(R.id.tv_TipoSeguro)
    TextView tvTipoSeguro;


    //Opcion camara
    private static String APP_DIRECTORY = "SinLimites/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "Imagenes";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private static final int READ_REQUEST_CODE = 400;

    private String mPath;
    private Uri uri_p;
    Long timestamp;

    private PersonDetail person;
    private UserPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_data_detail);
        ButterKnife.bind(this);

        presenter = new UserPresenter(person, getApplicationContext(),
                tvNomDetail, tvFechNacimientoDetail, tvGenero, tvDocIdentidadDetail, tvTelefonoDetail, tvUbicacion, tvDireccion,
                tvCodConadis, tvTipoDiscapacidades, tvTipoSeguro);
        //Obtener id de SharedPreferences...
        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        presenter.getDetail(registerManager.getIdPerson());

        showToolbar("Datos Personales", true);
    }



    @OnClick({R.id.btnEditDetail, R.id.iconEditDetail1, R.id.iconEditDetail2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnEditDetail:
                if (myRequestStoragePermissions()){
                    showOptions();
                    Log.d("TAG1","entro permissions");
                } else{
                    Log.d("TAG1","no entro permissions");
                }
                break;
            case R.id.iconEditDetail1:
                person = presenter.getPerson();
                if (person!=null)
                {
                    Intent intent = new Intent(getApplicationContext(), EditPersonalData1Activity.class);
                    intent.putExtra("intent_detail_edit1", person);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No se ha cargado los datos todavia", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.iconEditDetail2:
                person = presenter.getPerson();
                if (person!=null)
                {
                    Intent intent2 = new Intent(getApplicationContext(), EditPersonalData2Activity.class);
                    intent2.putExtra("intent_detail_edit2", person);
                    startActivity(intent2);
                } else {
                    Toast.makeText(getApplicationContext(), "No se ha cargado los datos todavia", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean myRequestStoragePermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if ((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED))

            return true;

        if ((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) ||
                shouldShowRequestPermissionRationale(CAMERA)) {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        }

        return false;
    }

    private void showOptions() {
        final CharSequence[] option = {"Tomar foto", "Elegir de galería", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(PersonalDataDetailActivity.this);

        builder.setTitle("Elige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (option[i] == "Tomar foto") {
                    openCamera();
                } else if (option[i] == "Elegir de galería") {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_PICTURE);
                } else {
                    dialogInterface.dismiss();
                }
            }
        });

        builder.show();
    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if (!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if (isDirectoryCreated) {
            timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(mPath);
            Uri photoURI = FileProvider.getUriForFile(PersonalDataDetailActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    newFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intent, PHOTO_CODE);
        }
    }


    //Event
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,
                            new String[]{mPath}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> Uri = " + uri);
                                    uri_p = uri;
                                    iniciarCrop(uri_p);
                                }
                            });
                    break;

                case SELECT_PICTURE:
                    uri_p = data.getData();
                    iniciarCrop(uri_p);
                    break;

                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    uri_p = result.getUri();
                    mPath = getRealPathFromURIPath(uri_p, PersonalDataDetailActivity.this);
                    profileImage.setImageURI(uri_p);
                    break;
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(PersonalDataDetailActivity.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                showOptions();
            } else {
                showExplanation();
            }
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PersonalDataDetailActivity.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Se necesitan permisos para realizar las acciones");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);

                startActivity(intent);
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
            }
        });

        builder.show();
    }

    //Extras
    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void iniciarCrop(Uri imageUri) {
        Rect cuadro = new Rect(300, 300, 300, 300);

        CropImage.activity(imageUri)
                .setBackgroundColor(1)
                .setMinCropResultSize(100, 100)
                .setInitialCropWindowRectangle(cuadro)
                .setActivityTitle("Editar foto")
                .setCropMenuCropButtonTitle("Guardar")
                .setAllowRotation(true)
                .setBorderLineColor(Color.RED)
                .start(this);
    }

    //Toolbar
    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarDatosPersonales);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

}
