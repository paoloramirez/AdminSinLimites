package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.Athlete;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonDetail;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonHealthUpdate;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;
import pe.luisvertiz.sinlimites_admin.presentation.presenter.UserPresenter;

public class EditPersonalData2Activity extends AppCompatActivity {


    boolean flagDiscap;
    boolean flagChVisual, flagChIntelectual, flagChFisica, flagChAuditiva;

    int disability = 0;
    int seguro = 0;
    @BindView(R.id.toolbar_actualizar_datos_2)
    Toolbar toolbarActualizarDatos2;
    @BindView(R.id.tv_error_conadis)
    TextView tvErrorConadis;
    @BindView(R.id.et_conadis)
    EditText etConadis;
    @BindView(R.id.iv_info)
    ImageView ivInfo;
    @BindView(R.id.tv_error_discap)
    TextView tvErrorDiscap;
    @BindView(R.id.ch_visual)
    AppCompatCheckBox chVisual;
    @BindView(R.id.ll_visual)
    LinearLayout llVisual;
    @BindView(R.id.ch_intelectual)
    AppCompatCheckBox chIntelectual;
    @BindView(R.id.ll_intelectual)
    LinearLayout llIntelectual;
    @BindView(R.id.ch_fisica)
    AppCompatCheckBox chFisica;
    @BindView(R.id.ll_fisica)
    LinearLayout llFisica;
    @BindView(R.id.ch_auditiva)
    AppCompatCheckBox chAuditiva;
    @BindView(R.id.ll_auditiva)
    LinearLayout llAuditiva;

    @BindView(R.id.tv_error_tipo)
    TextView tvErrorTipo;
    @BindView(R.id.ch_publico)
    AppCompatCheckBox chPublico;
    @BindView(R.id.ll_publico)
    LinearLayout llPublico;
    @BindView(R.id.ch_privado)
    AppCompatCheckBox chPrivado;
    @BindView(R.id.ll_privado)
    LinearLayout llPrivado;
    @BindView(R.id.btn_register2)
    AppCompatButton btnRegister2;

    private PersonDetail person;
    private UserPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_personal_data2);
        ButterKnife.bind(this);

        //Llenar datos iniciales a editar
        Intent intent = getIntent();
        person = (PersonDetail)intent.getSerializableExtra("intent_detail_edit2");

        initFlags();
        rellenarDatos();

        showToolbar("Actualizar datos", true);

        chVisual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChVisual = true;
                    tvErrorDiscap.setText("");
                    disability += 1;
                } else {
                    disability -= 1;
                    if (chAuditiva.isChecked() || chFisica.isChecked() || chIntelectual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChVisual = false;

                }
            }
        });

        chIntelectual.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChIntelectual = true;
                    tvErrorDiscap.setText("");
                    disability += 2;
                } else {
                    disability -= 2;
                    if (chAuditiva.isChecked() || chFisica.isChecked() || chVisual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChIntelectual = false;
                }
            }
        });

        chAuditiva.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChAuditiva = true;
                    disability += 8;
                } else {
                    disability -= 8;
                    if (chVisual.isChecked() || chFisica.isChecked() || chIntelectual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChAuditiva = false;
                }
            }
        });

        chFisica.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    flagChFisica = true;
                    disability += 4;
                } else {
                    disability -= 4;
                    if (chVisual.isChecked() || chAuditiva.isChecked() || chIntelectual.isChecked()) {
                        tvErrorDiscap.setText("");
                    } else {
                        tvErrorDiscap.setText("Seleccione su discapacidad");
                    }
                    flagChFisica = false;
                }

            }
        });

        chPublico.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    seguro += 1;
                } else {
                    seguro -= 1;
                }

            }
        });

        chPrivado.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    seguro += 2;
                } else {
                    seguro -= 2;
                }

            }
        });


    }

    private void rellenarDatos() {

        Athlete athlete = person.getAthlete();

        if (athlete.getPersonConadisCode() != null) {
            etConadis.setText(athlete.getPersonConadisCode());
        }
        //Rellenar Discapacidades
        rellenarDiscapacidades(athlete.getDisabilityType());
        flagDiscap=true;
        if (athlete.getHealthInsuranceType() != null) {
            //Rellenar tipos de seguro
            rellenarTipoSeguros(athlete.getHealthInsuranceType());
        }
    }

    private void rellenarTipoSeguros(List<Integer> tmp) {
        for (int i=0; i<tmp.size(); i++)
        {
            switch (tmp.get(i)) {
                case 1:
                    chPublico.setChecked(true);
                    seguro += 1;
                    break;
                case 2:
                    chPrivado.setChecked(true);
                    seguro += 2;
                    break;
            }
        }
    }

    private void rellenarDiscapacidades(List<Integer> tmp) {
        for (int i=0; i<tmp.size(); i++)
        {
            switch (tmp.get(i)) {
                case 1:
                    chVisual.setChecked(true);
                    disability += 1;
                    break;
                case 2:
                    chIntelectual.setChecked(true);
                    disability += 2;
                    break;
                case 3:
                    chFisica.setChecked(true);
                    disability += 4;
                    break;
                case 4:
                    chAuditiva.setChecked(true);
                    disability += 8;
                    break;
            }
        }
    }

    private void initFlags() {
        flagDiscap = false;
    }

    private void initErrors() {
        tvErrorDiscap.setText("Seleccione su discapacidad");
    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);

    }

    @OnClick({R.id.iv_info, R.id.ll_visual, R.id.ll_intelectual, R.id.ll_fisica, R.id.ll_auditiva, R.id.btn_register2, R.id.ll_publico, R.id.ll_privado})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.iv_info:
                showDialogConadis();
                break;
            case R.id.ll_visual:
                if (chVisual.isChecked()) {
                    chVisual.setChecked(false);
                } else {
                    chVisual.setChecked(true);
                }

                break;
            case R.id.ll_intelectual:
                if (chIntelectual.isChecked()) {
                    chIntelectual.setChecked(false);
                } else {
                    chIntelectual.setChecked(true);
                }
                break;
            case R.id.ll_fisica:
                if (chFisica.isChecked()) {
                    chFisica.setChecked(false);
                } else {
                    chFisica.setChecked(true);
                }
                break;
            case R.id.ll_auditiva:
                if (chAuditiva.isChecked()) {
                    chAuditiva.setChecked(false);
                } else {
                    chAuditiva.setChecked(true);
                }
                break;

            case R.id.btn_register2:

                if (chFisica.isChecked() || chAuditiva.isChecked() || chIntelectual.isChecked() || chVisual.isChecked()) {
                    guardarCambios();
                } else {
                    tvErrorDiscap.setText("Seleccione su discapacidad");
                    Toast.makeText(getApplicationContext(), "SELECCIONE SU DISCAPACIDAD", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ll_privado:
                break;

            case R.id.ll_publico:
                break;
        }

        if (chFisica.isChecked() || chVisual.isChecked() || chIntelectual.isChecked() || chAuditiva.isChecked()) {
            tvErrorDiscap.setText("");
        } else {
            tvErrorDiscap.setText("Seleccione su discapacidad");
        }
    }

    private void showDialogConadis() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.dialog_conadis, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    //Toolbar
    private void showToolbar(String title, boolean upButton) {
        setSupportActionBar(toolbarActualizarDatos2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(upButton);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                if (chFisica.isChecked() || chAuditiva.isChecked() || chIntelectual.isChecked() || chVisual.isChecked()) {
                    guardarCambios();
                } else {
                    tvErrorDiscap.setText("Seleccione su discapacidad");
                    Toast.makeText(getApplicationContext(), "SELECCIONE SU DISCAPACIDAD", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    private void guardarCambios() {
        presenter=new UserPresenter(getApplicationContext());
        PersonHealthUpdate personHealthUpdate = new PersonHealthUpdate();
        Athlete athlete = new Athlete();

        RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
        personHealthUpdate.setIdPerson(registerManager.getIdPerson());
        personHealthUpdate.setPersonConadisCode(etConadis.getText().toString());
        personHealthUpdate.setPersonDisabilityType(athlete.ToListDisability(disability));
        personHealthUpdate.setPersonHealthInsuranceType(athlete.ToListInsurance(seguro));

        presenter.personHealthUpdate(personHealthUpdate);
    }
}
