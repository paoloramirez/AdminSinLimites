package pe.luisvertiz.sinlimites_admin.presentation.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.entity.AthleteMin;
import pe.luisvertiz.sinlimites_admin.presentation.adapter.AthletesAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListAthletes1Fragment extends Fragment {

    private RecyclerView rvAthletes1;
    private AthletesAdapter adaptadorAthlete;

    private ArrayList<AthleteMin> athleteMins = new ArrayList<>();

    public ListAthletes1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_athletes1, container, false);
        //RecyclerAthlete
        rvAthletes1 = view.findViewById(R.id.rv_athletes1);
        rvAthletes1.setLayoutManager(new LinearLayoutManager(view.getContext()));
        //Obteniendo datos de los deportistas
        getAthletes();

        adaptadorAthlete = new AthletesAdapter(athleteMins, getActivity(), 1);
        rvAthletes1.setAdapter(adaptadorAthlete);

        return view;
    }

    private void getAthletes() {
        try {
            athleteMins = (ArrayList<AthleteMin>) getArguments().getSerializable("athletes_1");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
