package pe.luisvertiz.sinlimites_admin.presentation.contract;

/**
 * Created by Luis on 08/02/2018.
 */

public interface UbigeoContract {
    void getDepartments();
    void getProvinces(int idDepartment);
    void getDistricts(int idProvince);
}
