package pe.luisvertiz.sinlimites_admin.presentation.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import pe.luisvertiz.sinlimites_admin.R;
import pe.luisvertiz.sinlimites_admin.data.repository.local.session.RegisterManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    RegisterManager registerManager = RegisterManager.getInstance(getApplicationContext());
                    if(registerManager.isActive()){
                        launchActivity(getApplicationContext(), Main2Activity.class);
                    } else{
                        launchActivity(getApplicationContext(), LoginActivity.class);
                    }
                    finish();
                }

            }
        }).start();

    }

    private void launchActivity(Context context, Class clase) {
        Intent intent = new Intent(context, clase);
        startActivity(intent);

    }
}
