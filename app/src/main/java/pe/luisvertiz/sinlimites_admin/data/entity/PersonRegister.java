package pe.luisvertiz.sinlimites_admin.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luis on 08/02/2018.
 */

public class PersonRegister {
    @SerializedName("idUser")
    @Expose
    private int idUser;

    @SerializedName("personFirstName")
    @Expose
    private String firstName;

    @SerializedName("personLastName")
    @Expose
    private String lastName;

    @SerializedName("personBirthDate")
    @Expose
    private String birthdate;

    @SerializedName("personGender")
    @Expose
    private String gender;

    @SerializedName("personDocumentNumber")
    @Expose
    private int documentNumber;

    @SerializedName("personDocumentType")
    @Expose
    private int documentType;

    @SerializedName("personPhone")
    @Expose
    private int phone;

    @SerializedName("personAddress")
    @Expose
    private String address;

    @SerializedName("personConadisCode")
    @Expose
    private String codeConadis;

    @SerializedName("personDisabilityReason")
    @Expose
    private String disabilityReason;

    @SerializedName("personDistrict")
    @Expose
    private int district;

    @SerializedName("contactFirstName")
    @Expose
    private String contactFirstName;

    @SerializedName("contactLastName")
    @Expose
    private String contactLastName;

    @SerializedName("contactPhone")
    @Expose
    private String contactPhone;

    @SerializedName("contactEmail")
    @Expose
    private String contactEmail;

    @SerializedName("contactDocumentNumber")
    @Expose
    private String contactDocumentNumber;

    @SerializedName("contactDocumentType")
    @Expose
    private int contactDocumentType;

    @SerializedName("contactRelationship")
    @Expose
    private int contactRelationship;

    @SerializedName("contactRelationshipOther")
    @Expose
    private String contactRelationshipOther;

    @SerializedName("personSocialNetwork")
    @Expose
    private ArrayList<String> personSocialNetwork;

    @SerializedName("personTechnologicalDevice")
    @Expose
    private ArrayList<String> personTechnologicalDevice;

    @SerializedName("personFavoriteTechnologicalDevice")
    @Expose
    private String personFavoriteTechnologicalDevice;

    @SerializedName("personDisabilityType")
    @Expose
    private ArrayList<String> personDisabilityType;

    @SerializedName("personHealthInsurableType")
    @Expose
    private ArrayList<String> personHealthInsurableType;

    @SerializedName("personEventSport")
    @Expose
    private ArrayList<String> personEventSport;

    @SerializedName("personInterestedSport")
    @Expose
    private ArrayList<String> personInterestedSport;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(int documentNumber) {
        this.documentNumber = documentNumber;
    }

    public int getDocumentType() {
        return documentType;
    }

    public void setDocumentType(int documentType) {
        this.documentType = documentType;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCodeConadis() {
        return codeConadis;
    }

    public void setCodeConadis(String codeConadis) {
        this.codeConadis = codeConadis;
    }

    public String getDisabilityReason() {
        return disabilityReason;
    }

    public void setDisabilityReason(String disabilityReason) {
        this.disabilityReason = disabilityReason;
    }

    public int getDistrict() {
        return district;
    }

    public void setDistrict(int district) {
        this.district = district;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactDocumentNumber() {
        return contactDocumentNumber;
    }

    public void setContactDocumentNumber(String contactDocumentNumber) {
        this.contactDocumentNumber = contactDocumentNumber;
    }

    public int getContactDocumentType() {
        return contactDocumentType;
    }

    public void setContactDocumentType(int contactDocumentType) {
        this.contactDocumentType = contactDocumentType;
    }

    public int getContactRelationship() {
        return contactRelationship;
    }

    public void setContactRelationship(int contactRelationship) {
        this.contactRelationship = contactRelationship;
    }

    public String getContactRelationshipOther() {
        return contactRelationshipOther;
    }

    public void setContactRelationshipOther(String contactRelationshipOther) {
        this.contactRelationshipOther = contactRelationshipOther;
    }

    public ArrayList<String> getPersonSocialNetwork() {
        return personSocialNetwork;
    }

    public void setPersonSocialNetwork(ArrayList<String> personSocialNetwork) {
        this.personSocialNetwork = personSocialNetwork;
    }

    public ArrayList<String> getPersonTechnologicalDevice() {
        return personTechnologicalDevice;
    }

    public void setPersonTechnologicalDevice(ArrayList<String> personTechnologicalDevice) {
        this.personTechnologicalDevice = personTechnologicalDevice;
    }

    public ArrayList<String> getPersonDisabilityType() {
        return personDisabilityType;
    }

    public void setPersonDisabilityType(ArrayList<String> personDisabilityType) {
        this.personDisabilityType = personDisabilityType;
    }

    public ArrayList<String> getPersonHealthInsurableType() {
        return personHealthInsurableType;
    }

    public void setPersonHealthInsurableType(ArrayList<String> personHealthInsurableType) {
        this.personHealthInsurableType = personHealthInsurableType;
    }

    public ArrayList<String> getPersonEventSport() {
        return personEventSport;
    }

    public void setPersonEventSport(ArrayList<String> personEventSport) {
        this.personEventSport = personEventSport;
    }

    public ArrayList<String> getPersonInterestedSport() {
        return personInterestedSport;
    }

    public void setPersonInterestedSport(ArrayList<String> personInterestedSport) {
        this.personInterestedSport = personInterestedSport;
    }

    public String getPersonFavoriteTechnologicalDevice() {
        return personFavoriteTechnologicalDevice;
    }

    public void setPersonFavoriteTechnologicalDevice(String personFavoriteTechnologicalDevice) {
        this.personFavoriteTechnologicalDevice = personFavoriteTechnologicalDevice;
    }
}
