package pe.luisvertiz.sinlimites_admin.data.entity;

/**
 * Created by desarrollo03 on 15/02/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Contact implements Serializable {
    public Contact() {
    }

    @SerializedName("idContact")
    @Expose
    private Integer idContact;
    @SerializedName("contactFirstName")
    @Expose
    private String contactFirstName;
    @SerializedName("contactLastName")
    @Expose
    private String contactLastName;
    @SerializedName("contactPhone")
    @Expose
    private String contactPhone;
    @SerializedName("contactEmail")
    @Expose
    private String contactEmail;
    @SerializedName("contactDocumentNumber")
    @Expose
    private String contactDocumentNumber;
    @SerializedName("contactRelationshipOther")
    @Expose
    private String contactRelationshipOther;
    @SerializedName("idRelationship")
    @Expose
    private Integer idRelationship;
    @SerializedName("idDocumentType")
    @Expose
    private Integer idDocumentType;

    public Integer getIdContact() {
        return idContact;
    }

    public void setIdContact(Integer idContact) {
        this.idContact = idContact;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactDocumentNumber() {
        return contactDocumentNumber;
    }

    public void setContactDocumentNumber(String contactDocumentNumber) {
        this.contactDocumentNumber = contactDocumentNumber;
    }

    public String getContactRelationshipOther() {
        return contactRelationshipOther;
    }

    public void setContactRelationshipOther(String contactRelationshipOther) {
        this.contactRelationshipOther = contactRelationshipOther;
    }

    public Integer getIdRelationship() {
        return idRelationship;
    }

    public void setIdRelationship(Integer idRelationship) {
        this.idRelationship = idRelationship;
    }

    public Integer getIdDocumentType() {
        return idDocumentType;
    }

    public void setIdDocumentType(Integer idDocumentType) {
        this.idDocumentType = idDocumentType;
    }

}
