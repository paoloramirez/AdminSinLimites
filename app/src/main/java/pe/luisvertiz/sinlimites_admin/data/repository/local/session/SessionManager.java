package pe.luisvertiz.sinlimites_admin.data.repository.local.session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by desarrollo03 on 19/02/18.
 */

public class SessionManager {

    private static final String PREFERENCE_NAME = "SesionSP";
    private int PRIVATE_MODE = 0;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private static SessionManager sesionManager;

    public static SessionManager getInstance(Context context) {
        if ( sesionManager == null) {
            sesionManager = new SessionManager(context);
        }
        return sesionManager;
    }

    private SessionManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    /*
     * Sesion
     */

    public static final String KEY_SESSION="session";
    public static final String KEY_ID_USER="idUser";
    public static final String KEY_USER_TYPE="userType";

    public void setIdUser(int idUser){
        editor.putInt(KEY_ID_USER, idUser);
    }

    public int getIdUser(){
        return preferences.getInt(KEY_ID_USER, 0);
    }

    public void login(){
        editor.putBoolean(KEY_SESSION, true);
    }

    public boolean isActive(){
        return preferences.getBoolean(KEY_SESSION, false);
    }

    public void logOut(){
        editor.clear();
    }

    public void setIdUserType(int idUserType){
        editor.putInt(KEY_USER_TYPE, idUserType);
    }

    public int getIdUserType(){
        return preferences.getInt(KEY_USER_TYPE, 0);
    }
}
