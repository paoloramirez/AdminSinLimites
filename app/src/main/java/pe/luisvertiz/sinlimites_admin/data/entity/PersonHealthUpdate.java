package pe.luisvertiz.sinlimites_admin.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by desarrollo03 on 20/02/18.
 */

public class PersonHealthUpdate implements Serializable {

    @SerializedName("idPerson")
    @Expose
    private Integer idPerson;
    @SerializedName("personConadisCode")
    @Expose
    private String personConadisCode;
    @SerializedName("personDisabilityType")
    @Expose
    private List<Integer> personDisabilityType = null;
    @SerializedName("personHealthInsuranceType")
    @Expose
    private List<Integer> personHealthInsuranceType = null;

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getPersonConadisCode() {
        return personConadisCode;
    }

    public void setPersonConadisCode(String personConadisCode) {
        this.personConadisCode = personConadisCode;
    }

    public List<Integer> getPersonDisabilityType() {
        return personDisabilityType;
    }

    public void setPersonDisabilityType(List<Integer> personDisabilityType) {
        this.personDisabilityType = personDisabilityType;
    }

    public List<Integer> getPersonHealthInsuranceType() {
        return personHealthInsuranceType;
    }

    public void setPersonHealthInsuranceType(List<Integer> personHealthInsuranceType) {
        this.personHealthInsuranceType = personHealthInsuranceType;
    }
}
