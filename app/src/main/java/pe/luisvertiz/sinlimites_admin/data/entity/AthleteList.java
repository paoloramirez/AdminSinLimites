package pe.luisvertiz.sinlimites_admin.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by desarrollo03 on 21/02/18.
 */

public class AthleteList implements Serializable {

    public AthleteList() {
    }

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("athletes")
    @Expose
    private List<AthleteMin> athletes = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<AthleteMin> getAthletes() {
        return athletes;
    }

    public void setAthletes(List<AthleteMin> athletes) {
        this.athletes = athletes;
    }
}
