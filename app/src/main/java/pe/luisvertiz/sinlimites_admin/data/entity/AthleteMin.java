package pe.luisvertiz.sinlimites_admin.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Capsula01 on 1/02/2018.
 */

public class AthleteMin implements Serializable {

    public AthleteMin() {
    }

    @SerializedName("idPerson")
    @Expose
    private Integer idPerson;
    @SerializedName("personFirstName")
    @Expose
    private String personFirstName;
    @SerializedName("personLastName")
    @Expose
    private String personLastName;
    @SerializedName("personImageUrl")
    @Expose
    private String personImageUrl;
    @SerializedName("personQualifiedStatus")
    @Expose
    private Integer personQualifiedStatus;
    @SerializedName("idsQualifiedSport")
    @Expose
    private List<Integer> idsQualifiedSport = null;

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        this.personFirstName = personFirstName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonImageUrl() {
        return personImageUrl;
    }

    public void setPersonImageUrl(String personImageUrl) {
        this.personImageUrl = personImageUrl;
    }

    public Integer getPersonQualifiedStatus() {
        return personQualifiedStatus;
    }

    public void setPersonQualifiedStatus(Integer personQualifiedStatus) {
        this.personQualifiedStatus = personQualifiedStatus;
    }

    public List<Integer> getIdsQualifiedSport() {
        return idsQualifiedSport;
    }

    public void setIdsQualifiedSport(List<Integer> idsQualifiedSport) {
        this.idsQualifiedSport = idsQualifiedSport;
    }

    //Extras
    public String getSportstoString() {
        String sports = "";
        List<Integer> tmp = getIdsQualifiedSport();

        for (int i=0; i<tmp.size()-1; i++)
        {
            sports+= SportCodetoName(tmp.get(i)) + ", ";
        }

        sports+= SportCodetoName(tmp.get(tmp.size()-1)) + ".";

        return sports;
    }

    private String SportCodetoName(int code) {
        String name="";
        switch (code)
        {
            case 1:
                name="Fútbol";
                break;
            case 2:
                name="Vóley";
                break;
            case 3:
                name="Bochas";
                break;
            case 4:
                name="Atletismo";
                break;
        }

        return name;
    }

}
