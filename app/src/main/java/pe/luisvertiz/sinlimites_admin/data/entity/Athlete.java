package pe.luisvertiz.sinlimites_admin.data.entity;

/**
 * Created by desarrollo03 on 15/02/18.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Athlete implements Serializable{
    public Athlete() {
    }

    @SerializedName("idPerson")
    @Expose
    private Integer idPerson;
    @SerializedName("personFirstName")
    @Expose
    private String personFirstName;
    @SerializedName("personLastName")
    @Expose
    private String personLastName;
    @SerializedName("personBirthDate")
    @Expose
    private String personBirthDate;
    @SerializedName("personGender")
    @Expose
    private String personGender;
    @SerializedName("personPhone")
    @Expose
    private String personPhone;
    @SerializedName("personAddress")
    @Expose
    private String personAddress;
    @SerializedName("personConadisCode")
    @Expose
    private String personConadisCode;
    @SerializedName("personImageUrl")
    @Expose
    private String personImageUrl;
    @SerializedName("personDocumentNumber")
    @Expose
    private String personDocumentNumber;
    @SerializedName("idDocumentType")
    @Expose
    private Integer idDocumentType;
    @SerializedName("personQualifiedStatus")
    @Expose
    private Integer personQualifiedStatus;
    @SerializedName("personUbigeo")
    @Expose
    private PersonUbigeo personUbigeo;
    @SerializedName("disabilityType")
    @Expose
    private List<Integer> disabilityType = null;
    @SerializedName("healthInsuranceType")
    @Expose
    private List<Integer> healthInsuranceType = null;
    @SerializedName("personDisabilityReason")
    @Expose
    private String personDisabilityReason;

    /*
     * Metodos
     */

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        this.personFirstName = personFirstName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonBirthDate() {
        return personBirthDate;
    }

    public void setPersonBirthDate(String personBirthDate) {
        this.personBirthDate = personBirthDate;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }

    public String getPersonPhone() {
        return personPhone;
    }

    public void setPersonPhone(String personPhone) {
        this.personPhone = personPhone;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonConadisCode() {
        return personConadisCode;
    }

    public void setPersonConadisCode(String personConadisCode) {
        this.personConadisCode = personConadisCode;
    }

    public String getPersonImageUrl() {
        return personImageUrl;
    }

    public void setPersonImageUrl(String personImageUrl) {
        this.personImageUrl = personImageUrl;
    }

    public String getPersonDocumentNumber() {
        return personDocumentNumber;
    }

    public void setPersonDocumentNumber(String personDocumentNumber) {
        this.personDocumentNumber = personDocumentNumber;
    }

    public Integer getIdDocumentType() {
        return idDocumentType;
    }

    public void setIdDocumentType(Integer idDocumentType) {
        this.idDocumentType = idDocumentType;
    }

    public Integer getPersonQualifiedStatus() {
        return personQualifiedStatus;
    }

    public void setPersonQualifiedStatus(Integer personQualifiedStatus) {
        this.personQualifiedStatus = personQualifiedStatus;
    }

    public PersonUbigeo getPersonUbigeo() {
        return personUbigeo;
    }

    public void setPersonUbigeo(PersonUbigeo personUbigeo) {
        this.personUbigeo = personUbigeo;
    }

    public List<Integer> getDisabilityType() {
        return disabilityType;
    }

    public void setDisabilityType(List<Integer> disabilityType) {
        this.disabilityType = disabilityType;
    }

    public List<Integer> getHealthInsuranceType() {
        return healthInsuranceType;
    }

    public void setHealthInsuranceType(List<Integer> healthInsuranceType) {
        this.healthInsuranceType = healthInsuranceType;
    }

    public String getPersonDisabilityReason() {
        return personDisabilityReason;
    }

    public void setPersonDisabilityReason(String personDisabilityReason) {
        this.personDisabilityReason = personDisabilityReason;
    }


    //Extras
    public String getSportstoString(List<Integer> tmp) {
        String sports = "";

        for (int i=0; i<tmp.size()-1; i++)
        {
            sports+= SportCodetoName(tmp.get(i)) + ", ";
        }

        sports+= SportCodetoName(tmp.get(tmp.size()-1)) + ".";

        return sports;
    }

    public String getRelationshiptoDetail (int code, String others)
    {
        if (code == 3)
        {
            return others;
        }
        return RelationshipCodetoName(code);
    }

    public String getGenderDetail()
    {
        switch (getPersonGender())
        {
            case "M":
                return "Masculino";

            case "F":
                return "Femenino";
        }

        return "";
    }


    public String getDocIdentidadDetail()
    {
        String result = "";

        switch (getIdDocumentType())
        {
            case 1:
                result+="DNI - ";
                break;
            case 2:
                result+="Carnet de Extranejería - ";
                break;

            default:
                result="";
                break;
        }

        result+=getPersonDocumentNumber();

        return result;
    }

    public String getUbicacionDetail() {
        String result = "";
        result+=getPersonUbigeo().getDistrict().getName() + ", ";
        result+=getPersonUbigeo().getProvince().getName() + ", ";
        result+=getPersonUbigeo().getDepartment().getName();

        return result;
    }

    public String getDiscapacidadesDetail() {
        String result = "";
        List<Integer> tmp = getDisabilityType();

        for (int i=0; i<tmp.size()-1; i++)
        {
            result+= DisabilityCodetoName(tmp.get(i)) + ", ";
        }

        result+= DisabilityCodetoName(tmp.get(tmp.size()-1)) + ".";

        return result;
    }

    public String getSegurosDetail() {
        String result = "";
        List<Integer> tmp = getHealthInsuranceType();

        for (int i=0; i<tmp.size()-1; i++)
        {
            result+= InsuranceCodetoName(tmp.get(i)) + ", ";
        }

        result+= InsuranceCodetoName(tmp.get(tmp.size()-1)) + ".";

        return result;
    }

    //funciones de apoyo

    private String RelationshipCodetoName(int code) {
        String name="";
        switch (code)
        {
            case 1:
                name="Padre";
                break;
            case 2:
                name="Tutor";
                break;
            case 3:
                name="";
                break;
            default:
                name="";
                break;
        }

        return name;
    }

    private String SportCodetoName(int code) {
        String name="";
        switch (code)
        {
            case 1:
                name="Fútbol";
                break;
            case 2:
                name="Vóley";
                break;
            case 3:
                name="Bochas";
                break;
            case 4:
                name="Atletismo";
                break;
            default:
                name="";
                break;
        }

        return name;
    }

    private String DisabilityCodetoName(int code) {
        String name="";
        switch (code)
        {
            case 1:
                name="Visual";
                break;
            case 2:
                name="Intelectual";
                break;
            case 3:
                name="Física";
                break;
            case 4:
                name="Auditiva";
                break;
            default:
                name="";
                break;
        }

        return name;
    }

    private String InsuranceCodetoName(int code) {
        String name="";
        switch (code)
        {
            case 1:
                name="Público";
                break;
            case 2:
                name="Privado";
                break;
            default:
                name="";
                break;
        }

        return name;
    }

    //Funcion getData (Int to Array<Int>)
    public List<Integer> ToListDisability(int valor)
    {
        List<Integer> result = new ArrayList<>();
        int tmp = 1;

        for (int i=0; i < 5; i++)
        {
            int a = tmp & valor;
            if (a == tmp)
            {
                result.add(i+1);
            }
            tmp = tmp<<1;
        }

        return  result;
    }

    public List<Integer> ToListInsurance(int valor)
    {
        List<Integer> result = new ArrayList<>();
        int tmp = 1;

        for (int i=0; i < 5; i++)
        {
            int a = tmp & valor;
            if (a == tmp)
            {
                result.add(i+1);
            }
            tmp = tmp<<1;
        }

        return  result;
    }
}
