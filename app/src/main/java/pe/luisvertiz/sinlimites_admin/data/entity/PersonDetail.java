package pe.luisvertiz.sinlimites_admin.data.entity;

/**
 * Created by desarrollo03 on 15/02/18.
 */

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PersonDetail implements Serializable {
    public PersonDetail() {
    }

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("athlete")
    @Expose
    private Athlete athlete;
    @SerializedName("qualifiedSports")
    @Expose
    private List<Integer> qualifiedSports = null;
    @SerializedName("contact")
    @Expose
    private Contact contact;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Athlete getAthlete() {
        return athlete;
    }

    public void setAthlete(Athlete athlete) {
        this.athlete = athlete;
    }

    public List<Integer> getQualifiedSports() {
        return qualifiedSports;
    }

    public void setQualifiedSports(List<Integer> qualifiedSports) {
        this.qualifiedSports = qualifiedSports;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

}
