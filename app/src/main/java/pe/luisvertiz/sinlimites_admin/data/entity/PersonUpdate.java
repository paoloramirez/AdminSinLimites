package pe.luisvertiz.sinlimites_admin.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by desarrollo03 on 20/02/18.
 */

public class PersonUpdate implements Serializable {

    public PersonUpdate() {
    }

    @SerializedName("idPerson")
    @Expose
    private Integer idPerson;
    @SerializedName("personFirstName")
    @Expose
    private String personFirstName;
    @SerializedName("personLastName")
    @Expose
    private String personLastName;
    @SerializedName("personBirthDate")
    @Expose
    private String personBirthDate;
    @SerializedName("personGender")
    @Expose
    private String personGender;
    @SerializedName("personPhone")
    @Expose
    private String personPhone;
    @SerializedName("personAddress")
    @Expose
    private String personAddress;
    @SerializedName("personDocumentNumber")
    @Expose
    private String personDocumentNumber;
    @SerializedName("personDistrict")
    @Expose
    private Integer personDistrict;
    @SerializedName("personDocumentType")
    @Expose
    private Integer personDocumentType;

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        this.personFirstName = personFirstName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonBirthDate() {
        return personBirthDate;
    }

    public void setPersonBirthDate(String personBirthDate) {
        this.personBirthDate = personBirthDate;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }

    public String getPersonPhone() {
        return personPhone;
    }

    public void setPersonPhone(String personPhone) {
        this.personPhone = personPhone;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonDocumentNumber() {
        return personDocumentNumber;
    }

    public void setPersonDocumentNumber(String personDocumentNumber) {
        this.personDocumentNumber = personDocumentNumber;
    }

    public Integer getPersonDistrict() {
        return personDistrict;
    }

    public void setPersonDistrict(Integer personDistrict) {
        this.personDistrict = personDistrict;
    }

    public Integer getPersonDocumentType() {
        return personDocumentType;
    }

    public void setPersonDocumentType(Integer personDocumentType) {
        this.personDocumentType = personDocumentType;
    }

}
