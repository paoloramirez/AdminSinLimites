package pe.luisvertiz.sinlimites_admin.data.entity;

/**
 * Created by desarrollo03 on 15/02/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PersonUbigeo implements Serializable {
    public PersonUbigeo() {
    }

    public PersonUbigeo(District district, Province province, Department department) {
        this.district = district;
        this.province = province;
        this.department = department;
    }

    @SerializedName("district")
    @Expose
    private District district;
    @SerializedName("province")
    @Expose
    private Province province;
    @SerializedName("department")
    @Expose
    private Department department;

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

}