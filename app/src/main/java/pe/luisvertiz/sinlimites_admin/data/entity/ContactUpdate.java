package pe.luisvertiz.sinlimites_admin.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by desarrollo03 on 19/02/18.
 */

public class ContactUpdate implements Serializable {

    public ContactUpdate () {
    }

    @SerializedName("idPerson")
    @Expose
    private Integer idPerson;
    @SerializedName("contactFirstName")
    @Expose
    private String contactFirstName;
    @SerializedName("contactLastName")
    @Expose
    private String contactLastName;
    @SerializedName("contactPhone")
    @Expose
    private String contactPhone;
    @SerializedName("contactEmail")
    @Expose
    private String contactEmail="";
    @SerializedName("contactDocumentNumber")
    @Expose
    private String contactDocumentNumber;
    @SerializedName("contactRelationship")
    @Expose
    private Integer contactRelationship;
    @SerializedName("contactDocumentType")
    @Expose
    private Integer contactDocumentType;
    @SerializedName("contactRelationshipOther")
    @Expose
    private String contactRelationshipOther="";

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactDocumentNumber() {
        return contactDocumentNumber;
    }

    public void setContactDocumentNumber(String contactDocumentNumber) {
        this.contactDocumentNumber = contactDocumentNumber;
    }

    public Integer getContactRelationship() {
        return contactRelationship;
    }

    public void setContactRelationship(Integer contactRelationship) {
        this.contactRelationship = contactRelationship;
    }

    public Integer getContactDocumentType() {
        return contactDocumentType;
    }

    public void setContactDocumentType(Integer contactDocumentType) {
        this.contactDocumentType = contactDocumentType;
    }

    public String getContactRelationshipOther() {
        return contactRelationshipOther;
    }

    public void setContactRelationshipOther(String contactRelationshipOther) {
        this.contactRelationshipOther = contactRelationshipOther;
    }

}
