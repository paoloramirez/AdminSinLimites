package pe.luisvertiz.sinlimites_admin.data.repository.local.session;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by Luis on 06/02/2018.
 */

public class RegisterManager {
    private static final String PREFERENCE_NAME = "FileSP";
    private int PRIVATE_MODE = 0;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private static RegisterManager registerManager;

    public static RegisterManager getInstance(Context context) {
        if (registerManager == null) {
            registerManager = new RegisterManager(context);
        }
        return registerManager;
    }

    private RegisterManager(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    /*
     * Sesion
     */

    public static final String KEY_SESSION="session";
    public static final String KEY_ID_USER="idUser";
    public static final String KEY_ID_PERSON="idPerson";

    public void setIdUser(int idUser){
        editor.putInt(KEY_ID_USER, idUser);
    }

    public int getIdUser(){
        return preferences.getInt(KEY_ID_USER, 0);
    }

    public void setIdPerson(int idPerson){
        editor.putInt(KEY_ID_PERSON, idPerson);
    }

    public int getIdPerson(){
        return preferences.getInt(KEY_ID_PERSON, 0);
    }

    public void login(){
        editor.putBoolean(KEY_SESSION, true);
    }

    public boolean isActive(){
        return preferences.getBoolean(KEY_SESSION, false);
    }

    public void logOut(){
        editor.clear();
    }



    /*
     * Register1Activity
     */

    public static final String KEY_NAME="name";
    public static final String KEY_LAST_NAME="lastName";
    public static final String KEY_BIRTHDAY="birthday";
    public static final String KEY_GENERO="genero";
    public static final String KEY_CODE_TIPODOC="codeTipoDoc";
    public static final String KEY_NUMBER_DOCUMENT="numberDocument";
    public static final String KEY_TELEPHONE="telephone";
    public static final String KEY_CODE_DISTRICT="district";
    public static final String KEY_ADDRESS="address";

    public void setName(String name){
        editor.putString(KEY_NAME, name);
        editor.commit();
    }
    public String getName(){
        return preferences.getString(KEY_NAME, "");
    }

    public void setLastName(String lastName){
        editor.putString(KEY_LAST_NAME, lastName);
        editor.commit();
    }
    public String getLastName(){
        return preferences.getString(KEY_LAST_NAME, "");
    }

    public void setBirthday(String birthday){
        editor.putString(KEY_BIRTHDAY, birthday);
        editor.commit();
    }
    public String getBirthday(){
        return preferences.getString(KEY_BIRTHDAY, "");
    }

    public void setGenero(String genero){
        editor.putString(KEY_GENERO, genero);
        editor.commit();
    }
    public String getGenero(){
        return preferences.getString(KEY_GENERO, "");
    }

    public void setCodeTipoCod(int codeTipoCod){
        editor.putInt(KEY_CODE_TIPODOC, codeTipoCod);
        editor.commit();
    }
    public int getCodeTipoCod(){
        return preferences.getInt(KEY_CODE_TIPODOC, 0);
    }

    public void setNumberDocument(String numberDocument){
        editor.putString(KEY_NUMBER_DOCUMENT, numberDocument);
        editor.commit();
    }
    public String getNumberDocument(){
        return preferences.getString(KEY_NUMBER_DOCUMENT, "");
    }

    public void setTelephone(String telephone){
        editor.putString(KEY_TELEPHONE, telephone);
        editor.commit();
    }
    public String getTelephone(){
        return preferences.getString(KEY_TELEPHONE, "");
    }


    public void setCodeDistrict(int codeDistrict){
        editor.putInt(KEY_CODE_DISTRICT, codeDistrict);
        editor.commit();
    }
    public int getCodeDistrict(){
        return preferences.getInt(KEY_CODE_DISTRICT, 0);
    }

    public void setAddress(String address){
        editor.putString(KEY_ADDRESS, address);
        editor.commit();
    }
    public String getAddress(){
        return preferences.getString(KEY_ADDRESS, "");
    }


    /*
     * Register2Activity Atr - Methods
     */
    public static final String KEY_CODE_CONADIS="code_conadis";
    public static final String KEY_HISTORY="history";
    public static final String KEY_DISABILITY="disability";
    public static final String KEY_TYPE_SAFE="safe";

    public void setCodeConadis(String code){
        editor.putString(KEY_CODE_CONADIS, code);
        editor.commit();
    }
    public String getCodeConadis(){
        return preferences.getString(KEY_CODE_CONADIS, "");
    }

    public void setDisability(int disability){
        editor.putInt(KEY_DISABILITY, disability);
        editor.commit();
    }
    public ArrayList<String> getDisability(){
        int dato = preferences.getInt(KEY_DISABILITY, 0);
        return getDataChecks(dato, 0, "");
    }

    public void setHistory(String history){
        editor.putString(KEY_HISTORY, history);
        editor.commit();
    }
    public String getHistory(){
        return preferences.getString(KEY_HISTORY, "");
    }

    public void setTypeSafe(int tsafe){
        editor.putInt(KEY_TYPE_SAFE, tsafe);
        editor.commit();
    }
    public ArrayList<String> getTypeSafe(){
        int dato = preferences.getInt(KEY_TYPE_SAFE, 0);
        return getDataChecks(dato, 0, "");
    }

    /*
     * InterestsActivity Atr - Methods
     */
    public static final String KEY_SOCIAL_NETWORK="social_network";
    public static final String KEY_DEVICES="devices";
    public static final String KEY_PREFERENCE_DEVICE="preferences_devices";
    public static final String KEY_SPORTS="sports";
    public static final String KEY_PREFERENCES_SPORTS="preferences_sports";

    public static final String KEY_OTHERS_SOCIAL_NETWORK="others_social_network";
    public static final String KEY_OTHERS_PREFERENCES_SPORTS="others_preferences_sports";


    public void setSocialNetwork(int sn){
        editor.putInt(KEY_SOCIAL_NETWORK, sn);
        editor.commit();
    }
    public ArrayList<String> getSocialNetwork(){
        int dato = preferences.getInt(KEY_SOCIAL_NETWORK, 0);
        return getDataChecks(dato, 4, KEY_OTHERS_SOCIAL_NETWORK);
    }

    public void setDevices(int devices){
        editor.putInt(KEY_DEVICES, devices);
        editor.commit();
    }
    public ArrayList<String> getDevices(){
        int dato = preferences.getInt(KEY_DEVICES, 0);
        return getDataChecks(dato, 0, "");
    }

    public void setPreferenceDevice(int pdevice){
        editor.putInt(KEY_PREFERENCE_DEVICE, pdevice);
        editor.commit();
    }
    public int getPreferenceDevice(){
        return preferences.getInt(KEY_PREFERENCE_DEVICE, 0);
    }

    public void setSports(int sports){
        editor.putInt(KEY_SPORTS, sports);
        editor.commit();
    }
    public ArrayList<String> getSports(){
        int dato = preferences.getInt(KEY_SPORTS, 0);
        return getDataChecks(dato, 0, "");
    }

    public void setPreferencesSports(int psports){
        editor.putInt(KEY_PREFERENCES_SPORTS, psports);
        editor.commit();
    }
    public ArrayList<String> getPreferencesSports(){
        int dato = preferences.getInt(KEY_PREFERENCES_SPORTS, 0);
        return getDataChecks(dato, 5, KEY_OTHERS_PREFERENCES_SPORTS);
    }

    public void setOthersSocialNetwork(String osn){
        editor.putString(KEY_OTHERS_SOCIAL_NETWORK, osn);
        editor.commit();
    }
    public String getOthersSocialNetwork(){
        return preferences.getString(KEY_OTHERS_SOCIAL_NETWORK, "");
    }

    public void setOthersPreferencesSports(String psp){
        editor.putString(KEY_OTHERS_PREFERENCES_SPORTS, psp);
        editor.commit();
    }
    public String getOthersPreferencesSports(){
        return preferences.getString(KEY_OTHERS_PREFERENCES_SPORTS, "");
    }



    /*
     * ContactActivity
     */

    public static final String KEY_CONTACT_NAME_="name_contact";
    public static final String KEY_CONTACT_LAST_NAME="lastName_contact";
    public static final String KEY_CONTACT_CODE_TIPODOC="codeTipoDoc_contact";
    public static final String KEY_CONTACT_NUMBER_DOCUMENT="numberDocument_contact";
    public static final String KEY_CONTACT_TELEPHONE="telephone_contact";
    public static final String KEY_CONTACT_EMAIL="email_contact";
    public static final String KEY_TYPE_RELATIONSHIP="type_relationship";
    public static final String KEY_OTHER_TYPE_RELATIONSHIP="other_type_relationship";

    public void setContactName(String namec){
        editor.putString(KEY_CONTACT_NAME_, namec);
        editor.commit();
    }
    public String getContactName(){
        return preferences.getString(KEY_CONTACT_NAME_, "");
    }

    public void setContactLastName(String clastName){
        editor.putString(KEY_CONTACT_LAST_NAME, clastName);
        editor.commit();
    }
    public String getContactLastName(){
        return preferences.getString(KEY_CONTACT_LAST_NAME, "");
    }

    public void setContactCodeTipoCod(int ccodeTipoCod){
        editor.putInt(KEY_CONTACT_CODE_TIPODOC, ccodeTipoCod);
        editor.commit();
    }
    public int getContactCodeTipoCod(){
        return preferences.getInt(KEY_CONTACT_CODE_TIPODOC, 0);
    }

    public void setContactNumberDocument(String cnumberDocument){
        editor.putString(KEY_CONTACT_NUMBER_DOCUMENT, cnumberDocument);
        editor.commit();
    }
    public String getContactNumberDocument(){
        return preferences.getString(KEY_CONTACT_NUMBER_DOCUMENT, "");
    }

    public void setContactTelephone(String ctelephone){
        editor.putString(KEY_CONTACT_TELEPHONE, ctelephone);
        editor.commit();
    }
    public String getContactTelephone(){
        return preferences.getString(KEY_CONTACT_TELEPHONE, "");
    }

    public void setContactEmail(String cemail){
        editor.putString(KEY_CONTACT_EMAIL, cemail);
        editor.commit();
    }
    public String getContactEmail(){
        return preferences.getString(KEY_CONTACT_EMAIL, "");
    }

    public void setTypeRelationship(int relationship){
        editor.putInt(KEY_TYPE_RELATIONSHIP, relationship);
        editor.commit();
    }
    public int getTypeRelationship(){
        return preferences.getInt(KEY_TYPE_RELATIONSHIP, 0);
    }

    public void setContactOtherRelationship(String rs){
        editor.putString(KEY_OTHER_TYPE_RELATIONSHIP, rs);
        editor.commit();
    }
    public String getContactOtherRelationship(){
        return preferences.getString(KEY_OTHER_TYPE_RELATIONSHIP, "");
    }


    /*
     * Utilitarios
     */

    /*
     * EXTRAS
     */

    //Funcion getData (Int to Binary)
    private ArrayList<String> getDataChecks(int valor, int otros, String key)
    {
        ArrayList<String> result = new ArrayList<>();
        int tmp = 1;
        int cont = 1;

        for (int i=0; i < 5; i++)
        {
            int a = tmp & valor;
            if (a == tmp)
            {
                if (cont != otros) result.add(Integer.toString(cont));
                else result.add(getOtherData(key));
            }
            cont++;
            tmp = tmp<<1;
        }

        return  result;
    }

    private String getOtherData(String key) {return preferences.getString(key, "");
    }

}
