package pe.luisvertiz.sinlimites_admin.data.repository.remote.request;

import com.google.gson.JsonObject;

import pe.luisvertiz.sinlimites_admin.data.entity.AthleteList;
import pe.luisvertiz.sinlimites_admin.data.entity.ContactUpdate;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonDetail;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonHealthUpdate;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonRegister;
import pe.luisvertiz.sinlimites_admin.data.entity.PersonUpdate;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Luis on 07/02/2018.
 */

public interface UserRequest {
    @FormUrlEncoded
    @POST("api/v1/user/register")
    Call<JsonObject> signUp(@Field("userEmail") String email, @Field("userPassword") String password);


    @FormUrlEncoded
    @POST("api/v1/user/login")
    Call<JsonObject> login(@Field("userEmail") String email, @Field("userPassword") String password);

    @POST("api/v1/person")
    Call<JsonObject> register(@Body PersonRegister personRegister);

    @GET("api/v1/person")
    Call<AthleteList> getAll();

    //Detalle deportista
    @GET("api/v1/person/{id}")
    Call<PersonDetail> getDetail(@Path("id") int id);

    @PUT("api/v1/person/contact")
    Call<JsonObject> contactUpdate(@Body ContactUpdate contactUpdate);

    @PUT("api/v1/person/personal")
    Call<JsonObject> personUpdate(@Body PersonUpdate personUpdate);

    @PUT("api/v1/person/health")
    Call<JsonObject> personHealthUpdate(@Body PersonHealthUpdate personHealthUpdate);

}
