package pe.luisvertiz.sinlimites_admin.data.repository.remote.request;

import java.util.List;

import pe.luisvertiz.sinlimites_admin.data.entity.Department;
import pe.luisvertiz.sinlimites_admin.data.entity.District;
import pe.luisvertiz.sinlimites_admin.data.entity.Province;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Luis on 08/02/2018.
 */

public interface UbigeoRequest {
    @GET("api/v1/location/departments")
    Call<List<Department>> getDepartments();

    @GET("api/v1/location/departments/{id}/provinces")
    Call<List<Province>> getProvinces(@Path("id") int idDepartment);

    @GET("api/v1/location/departments/province/{id}/districts")
    Call<List<District>> getDistricts(@Path("id") int idProvince);


}
