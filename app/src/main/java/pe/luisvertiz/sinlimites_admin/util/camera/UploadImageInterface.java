package pe.luisvertiz.sinlimites_admin.util.camera;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Luis on 08/02/2018.
 */

public interface UploadImageInterface {
    @Multipart
    @POST("public_pruebaimagen/image/index.php")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file, @Part("file") RequestBody name);

}
